const words = ["Собака", "Пантера", "Леопард", "Корова"];
const regExprOneUpLetter = /^[А-Я]{1}/g;

const btnPlay = document.getElementById("btn-play");
const divLetters = document.getElementById("div-letters");
const divLetterField = document.getElementById("div-letterfield");
const inputLetter = document.getElementById("input-letter");
const btnLetter = document.getElementById("btn-submit-letter");
const divResult = document.getElementById("div-result");
const divGameInfo = document.getElementById("div-gameinfo");

const msgGuessed = "Ти вгадав!", msgNotGuessed = "Немає такої букви", 
    msgErrorLetter = "Введи, будь ласка, букву з українського алфавіту!";
let word = "";

class Word {
    #word_to_guess;
    #word_with_guessed_letter;
    #empty_letter = '_';
    #check_pattern = '/[А-Яа-яїіє]{1}/g';
    constructor(word) {
        this.#word_to_guess = word.toUpperCase();
        this.#word_with_guessed_letter = [];
        for(let i = 0; i < word.length; i++) {
            this.#word_with_guessed_letter[i] = this.#empty_letter;
        }
        console.log(this.#word_to_guess + ' ' + this.#word_with_guessed_letter);
    }
    getWordToGuess() {
        return this.#word_to_guess;
    }
    getWordWithGuessedLetter() {
        return this.#word_with_guessed_letter;
    }
    guessLetter(letter) {
        if (this.#word_with_guessed_letter.indexOf(letter) === -1) {
            let index = -1;
            if((index = this.#word_to_guess.indexOf(letter)) === -1) {
                //say to used "You don't guess!"
                changeMessage(msgNotGuessed);
                inputLetter.focus();
                return false;
            } else {
                changeMessage(msgGuessed);
                this.#word_with_guessed_letter[index] = letter;
                showLetter(index, letter);
                while ((index = this.#word_to_guess.indexOf(letter, index + 1) ) !== -1) {
                    showLetter(index, letter);
                    this.#word_with_guessed_letter[index] = letter;
                }
                inputLetter.focus();
                inputLetter.value = "";
                return true;
            }
        }
    }
    getWordLength() {
        return this.#word_to_guess.length;
    }
    isWordGuessed() {
        return this.#word_with_guessed_letter.indexOf(this.#empty_letter) === -1;
    }
    checkLetter(letter) {
        if (letter !== null && letter.match(this.#check_pattern)) {
            console.log("One symbol");
        }
    }
}

function generateWord() {
    const index = Math.floor(Math.random() * words.length);
    console.log(index);
    word = new Word(words[index]);
    console.log(word);
}

function createLayout() {
    if (divLetters.childElementCount != 0) {
        const elements = [...divLetters.children];
        for (let e of elements) {
            e.remove();
        }
    }

    for (let i = 0; i < word.getWordLength(); i++) {
        const divLetter = document.createElement("input");
        divLetter.classList.add("one-letter");
        divLetter.disabled = true;
        divLetters.append(divLetter);
    }
}

function changeMessage(msg) {
    divResult.innerText = msg;
}

function showLetter(index, letter) {
    if (divLetters.childElementCount !== 0) {
        divLetters.children[index].value = letter;
    }
}

btnPlay.onclick = () => {
    generateWord();
    createLayout();
    divLetterField.style.visibility = "visible";
    divResult.innerText = "";
    divGameInfo.innerText = "";
}

btnLetter.onclick = () => {
    console.log(inputLetter.value);
    let value = inputLetter.value;
    value = value.toUpperCase();
    if (value.match(regExprOneUpLetter) !== null) {
        if (word.guessLetter(value) && word.isWordGuessed()) {
            divGameInfo.innerText = "You win the game!";
            divLetterField.style.visibility = "hidden";
        }
    } else {
        changeMessage(msgErrorLetter);
        inputLetter.focus();
    }

}
