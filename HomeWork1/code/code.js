/*Task 1*/

var userName, city, age;

userName = prompt("Enter your name:");
city = prompt("Enter your city:");
age = prompt("Enter your age:");

document.write("<i>User</i><br/><br/>");
document.write("<b>Name:</b>&ensp;" + userName + "<br/>");
document.write("<b>City:</b>&ensp;" + city + "<br/>");
document.write("<b>Age:</b>&ensp;" + age + "<br/><br/>");
document.write("<i>End</i><br/>");

console.log("User:");
console.log("Name:\t" + userName);
console.log("City:\t" + city);
console.log("Age:\t" + age);
console.log("End of output");

/*Task 2*/

document.write("<br/><br/><b>Arifmetic operations</b><br/><br/>");

var  x = 6, y = 14, z = 4;
var td = "<td>", tdClose = "</td>", tr = "<tr>", trClose = "</tr>";

x += y - x++ * z;
document.write("<table>" + tr);
document.write(td + "x += y - x++ * z" + tdClose);
document.write(td + "x=" + x + "&ensp;y=" + y + "&ensp;z=" + z + tdClose + trClose);
// 1. x++
// 2. * z
// 3. y -
// 4. x +=

x = 6, y = 14, z = 4;
z = --x - y * 5;
document.write(tr + td + "z = --x - y * 5" + tdClose);
document.write(td + "x=" + x + "&ensp;y=" + y + "&ensp;z=" + z + tdClose + trClose);
// 1. --x
// 2. y * 5
// 3. -
// 4. =

x = 6, y = 14, z = 4;
y /= x + 5 % z;
document.write(tr + td + "y /= x + 5 % z" + tdClose);
document.write(td + "x=" + x + "&ensp;y=" + y + "&ensp;z=" + z + tdClose + trClose);
// 1. 5 % z
// 2. +
// 3. y /=

x = 6, y = 14, z = 4;
z - x++ + y * 5;
document.write(tr + td + "z - x++ + y * 5" + tdClose);
document.write(td + "x=" + x + "&ensp;y=" + y + "&ensp;z=" + z + tdClose + trClose);
// 1. x++
// 2. y * 5
// 3. +
// 4. -

x = 6, y = 14, z = 4;
x = y - x++ * z;
document.write(tr + td + "x = y - x++ * z" + tdClose);
document.write(td + "x=" + x + "&ensp;y=" + y + "&ensp;z=" + z + tdClose + trClose);
// 1. x++
// 2. * z
// 3. y -
// 4. x =
document.write("</table>");