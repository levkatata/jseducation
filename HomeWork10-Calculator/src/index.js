const regexprNumber = /[\d\.]{1}/g;
const regexprOperator = /^[\+\-\*\/]{1}$/g;
//const regexpEqual = /[m+-]{1}$/g;

class Calculator {
    #value1 = "0";
    #value2 = "0";
    #sign = undefined;
    #result = undefined;
    #memory = 0;

    #display = undefined;
    #btnEquals = undefined;
    #divM = undefined;

    constructor(display, btnEquals) {
        this.#display = display;
        this.#btnEquals = btnEquals;
        this.#divM = document.querySelector(".memory");
        this.show("0");
    }

    calculate () {
        if (this.#sign === undefined) {
            return false;
        }

        let v1 = Number.parseFloat(this.#value1),
            v2 = Number.parseFloat(this.#value2);

        switch(this.#sign) {
            case "+":
                this.#result = v1 + v2;
                break;
            case "-":
                this.#result = v1 - v2;
                break;
            case "*":
                this.#result = v1 * v2;
                break;
            case "/":
                if(v2 !== 0) {
                    this.#result = v1 / v2;
                } else {
                    setTimeout(() => { alert("You can't divide by zero!")}, 500);
                    return false;
                }
                break;
        }
        this.#value1 = this.#result;
        return true;
    }

    parseValue(value) {
        if (value != null && value !== undefined) {
            if (value.match(regexprNumber)) {
                console.log(value.match(regexprNumber));

                if (value === "." && this.#display.value.indexOf('.') != -1) {
                    return;
                } 

                if (value === "0" && this.#display.value === "0") {
                    return;
                }
                
                if(this.#sign === undefined) {
                    //if there is a value in result and value1, and the user press number, so he starts a new calculation
                    if (this.#value1 === this.#result) {
                        this.resetCalculation();
                    }

                    this.#value1 = this.#value1 === "0" && value != "." ? value : this.#value1 + value; 

                    this.show(this.#value1);
                } else {
                    this.#value2 = this.#value2 === "0" && value !== "." ? value : this.#value2 + value; 

                    this.show(this.#value2);
                    this.#btnEquals.disabled = false;
                }

            } else if (value.match(regexprOperator)) {
                console.log(value.match(regexprOperator));

                if (this.#value1 !== "0" && this.#value2 !== "0") {
                    if (this.calculate()) {
                        this.#value2 = "0";
                        this.show(this.#value1);
                    } else {
                        this.resetCalculation();
                    }
                }

                this.#sign = value;
            } else if (value === "=") {

                if (this.calculate()) {
                    this.#sign = undefined;
                    this.#value2 = "0";
                    this.show (this.#result);

                    this.#btnEquals.disabled = true;
                } else {
                    this.resetCalculation();
                }

            } else if (value === "C") {
                this.resetCalculation();
            } else if (value === "m+") {
                this.#memory += Number.parseFloat(this.#display.value);
                this.showM();
            } else if (value === "m-") {
                this.#memory -= Number.parseFloat(this.#display.value);
                this.showM();
            } else if (value === "mrc") {
                //this.resetCalculation();
                this.show(this.#memory);
                if (this.#value1 === this.#memory) {
                    this.#memory = 0;
                } else {
                    this.#value1 = this.#memory;
                }
                this.showM();
            }
        }
    }
    
    show (value) {
        console.log(this.#display.value);
        this.#display.value = value;
    }

    resetCalculation() {
        this.#value1 = "0";
        this.#value2 = "0";
        this.#sign = undefined;
        this.#result = undefined;
        this.show ("0");

        this.#btnEquals.disabled = true;
    }

    showM() {
        if (this.#memory != 0) {
            this.#divM.style.visibility = "visible";
            this.#display.classList.add("memory-input");
        } else {
            this.#divM.style.visibility = "hidden";
            this.#display.classList.remove("memory-input");
        }
    }
}


window.addEventListener("DOMContentLoaded",()=>{
    const btn = document.querySelector(".keys"),
        display = document.querySelector(".display > input"),
        btnEquals = document.querySelector("input[value='=']");
    const calculator = new Calculator(display, btnEquals);

    btn.addEventListener("click", function (e) {
        let value = e.target.value;
        calculator.parseValue(value);
    })
});



