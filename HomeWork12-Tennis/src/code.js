const KEY_UP_CODE = "ArrowUp",
    KEY_DOWN_CODE = "ArrowDown",
    ROCKET_STEP = 5,
    COMP_MOVE_UP = -5, COMP_MOVE_DOWN = 5,
    BALL_STEP = 13, BALL_STEP_FALL = 5, 
    BALL_RIGHT = 1, BALL_LEFT = -1, BALL_STOP = 0,
    BALL_UP = -1, BALL_DOWN = 1;

const WIN_BALL_COUNT = 5;

const opponents = ["Darth Vader", "Luke Skywalker", "Han Solo", "Obi-Wan Kenobi", 
    "Yoda", "Princess Leia", "R2-D2", "Chewbacca"];

class Ball {
    constructor(rocketWidth, rocketHeight, rocketMargin, mainFieldWidth, mainFieldHeight, game) {
        this.divBall = document.querySelector("#ball");
        this.divBall.style.display = "block";
        this.rocketHeight = convertPxToNumber(rocketHeight);
        this.ballHeigthHalf = Math.floor(this.divBall.offsetHeight * 0.9);

        this.curLeftBall = this.divBall.offsetLeft; 
        this.curTopBall = this.divBall.offsetTop; 
        this.curBallDirection = BALL_RIGHT;
        this.curBallFalling = BALL_DOWN;

        this.minLeftBallPos = convertPxToNumber(rocketWidth) + convertPxToNumber(rocketMargin) + 2;
        this.maxLeftBallPos = convertPxToNumber(mainFieldWidth) - 
            this.minLeftBallPos - this.divBall.offsetWidth - 1;
        this.minTopBallPos = 0;
        this.maxTopBallPos = convertPxToNumber(mainFieldHeight) - this.divBall.offsetHeight; 

        this.computerNewSetLeftPosition = this.minLeftBallPos + 20;
        this.playerNewSetLeftPosition = this.maxLeftBallPos - 20;

        this.game = game;
        this.myRocket = document.querySelector("#rocket-my");
        this.compRocket = document.querySelector("#rocket-comp");
        this.ballInterval = undefined;
    
        console.log("About ball:", this.minLeftBallPos, this.maxLeftBallPos);
    }

    moveBall() {
        let newLeftPos = this.curLeftBall;
        if (this.curBallDirection === BALL_RIGHT) {
            newLeftPos += BALL_STEP;
            if (newLeftPos > this.maxLeftBallPos) {
                //Right side is my rocket side

                //Working with ball height to allow repel the ball even if it partialy touch the rocket (being a little bit higher or lower)
                const topRocket = this.myRocket.offsetTop - this.ballHeigthHalf, 
                    bottomRocket = topRocket + this.rocketHeight + this.ballHeigthHalf;
                if (this.curTopBall >= topRocket && this.curTopBall <= bottomRocket) {
                    //Player repel the ball
                    newLeftPos = this.maxLeftBallPos;  
                    this.curBallDirection = BALL_LEFT;
                } else {
                    //GOAL TO ME!!!
                    console.log("GOAL TO ME!!!");
                    this.stop();
                    this.curBallDirection = BALL_STOP;
                    this.curLeftBall = newLeftPos;
                    this.divBall.style.left = `${newLeftPos}px`;
                    this.game.addCompGoal();
                    //this.divMyGoals.classList.add("goal-background");

                    if (this.game.checkWinner() === false) {
                        setTimeout(() => {
                            this.newSet(BALL_RIGHT);
                            //this.divMyGoals.classList.remove("goal-background");
                        }, 1000);
                    }
                    return;
                }
            } 
        } else if (this.curBallDirection === BALL_LEFT) {
            newLeftPos -= BALL_STEP;
            if (newLeftPos < this.minLeftBallPos) {
                //Left side is a computer side
        
                const topRocket = this.compRocket.offsetTop - this.ballHeigthHalf, 
                    bottomRocket = topRocket + this.rocketHeight + this.ballHeigthHalf;
                if (this.curTopBall >= topRocket && this.curTopBall <= bottomRocket) {
                    //Computer repels the ball
                    newLeftPos = this.minLeftBallPos;
                    this.curBallDirection = BALL_RIGHT;
                } else {
                    //GOAL TO COMPUTER!
                    this.stop();
                    this.curBallDirection = BALL_STOP;
                    this.curLeftBall = newLeftPos;
                    this.divBall.style.left = `${newLeftPos}px`;
                    this.game.addMyGoal();
            
                    if (this.game.checkWinner() === false) {
                        setTimeout(() => {
                            this.newSet(BALL_LEFT);
                            //this.divCompGoals.classList.remove("goal-background");
                        }, 700);
                    }
                    return;
                }
            }
        }

        this.curLeftBall = newLeftPos;
        this.divBall.style.left = `${newLeftPos}px`;

        // console.log("Ball move:");
        // console.dir(this.divBall);

        let newTopPos = this.curTopBall;
        if (this.curBallFalling === BALL_DOWN) {
            newTopPos += BALL_STEP_FALL;
            if (newTopPos > this.maxTopBallPos) {
                newTopPos = this.maxTopBallPos;
                this.curBallFalling = BALL_UP;
            }
        } else if (this.curBallFalling === BALL_UP) {
            newTopPos -= BALL_STEP_FALL;
            if (newTopPos < this.minTopBallPos) {
                newTopPos = this.minTopBallPos;
                this.curBallFalling = BALL_DOWN;
            }
        }

        this.curTopBall = newTopPos;
        this.divBall.style.top = `${newTopPos}px`;
    }
    
    newSet(direction) {
        if (this.ballInterval === undefined) {
            let left, top;
            this.divBall.style.top = `${top = this.generateRadomTopPostion()}px`;
            this.divBall.style.left = `${left = direction === BALL_LEFT ? this.playerNewSetLeftPosition : this.computerNewSetLeftPosition}px`;
            this.divBall.classList.remove('goal-ball');
            this.curBallDirection = direction;
            this.curBallFalling = BALL_UP;
            this.curLeftBall = left;
            this.curTopBall = top;
    
            setTimeout (this.newServe, 1500, this);
        }
    }

    generateRandomSpeed() {
        return Math.floor(Math.random() * 4) * 25 + 50;
    }

    generateRadomTopPostion() {
        return Math.floor(Math.random() * this.maxTopBallPos);
    }

    stop() {
        clearInterval(this.ballInterval);
        this.ballInterval = undefined;
    }

    newServe(ball) {
        //here we generate random speed - this is interval timeout: 100, 200, 300, 400
        const speed = ball.generateRandomSpeed();
        
        ball.ballInterval = setInterval(() => {
            ball.moveBall();
        }, speed);
    }
}

class Game {
    constructor() {
        this.divMainField = document.querySelector("#game-field");
        this.mainFieldWidth = this.divMainField.offsetWidth; 

        this.myGoals = 0;
        this.compGoals = 0;
        this.divMyGoals = document.querySelector("#my-goals");
        this.divCompGoals = document.querySelector("#comp-goals");

        this.divMyGoals.innerText = '0';
        this.divCompGoals.innerText = '0';

        this.ball = undefined;
        this.intervalRocket = undefined;

        this.divPlayerNameComp = document.querySelector("#comp-name");
        this.divPlayerNameMy = document.querySelector("#my-name");
    }

    addMyGoal() {
        this.myGoals++;
        this.divMyGoals.innerText = this.myGoals;
    }

    addCompGoal() {
        this.compGoals++;
        this.divCompGoals.innerText = this.compGoals;
    }

    resetGame() {
        this.myGoals = 0;
        this.compGoals = 0;
        this.divMyGoals.innerText = '0';
        this.divCompGoals.innerText = '0';
        let winnerLabel = this.divPlayerNameMy.previousElementSibling || this.divPlayerNameComp.previousElementSibling;
        if (winnerLabel !== null) {
            winnerLabel.remove();
        }
    }

    beginGame() {
        if (this.compRocket != null && this.compRocket.isMoving()) {
            this.stopGame();
            return;
        }

        const divMyRocket = document.querySelector("#rocket-my"),
            divCompRocket = document.querySelector("#rocket-comp");
    
        const rocketStyle = getComputedStyle(divMyRocket),
            mainFieldStyle = getComputedStyle(this.divMainField);
    
        const minTopPosition = 0,
            rocketLength = divMyRocket.offsetHeight, //convertPxToNumber(rocketStyle.height),
            maxTopPosition = this.divMainField.clientHeight - rocketLength; //convertPxToNumber(mainFieldStyle.height) - rocketLength;

        this.ball = new Ball(rocketStyle.width, rocketStyle.height, rocketStyle.marginRight,
            mainFieldStyle.width, mainFieldStyle.height, this);
        this.compRocket = new CompRocket(minTopPosition, maxTopPosition, divCompRocket, this.ball);
        
        this.resetGame();
        this.fillPlayerCompName();
        this.askPlayerName();
    
        document.addEventListener("keydown", (e) => {
            if(!e) {
                e = window.event;
            }
    
            if (e.key === KEY_UP_CODE) {
                let newTop = divMyRocket.offsetTop - ROCKET_STEP; //convertPxToNumber(rocketStyle.top) - ROCKET_STEP;
                newTop = newTop > minTopPosition ? newTop : minTopPosition;
                divMyRocket.style.top = `${newTop}px`;
            } else if (e.key === KEY_DOWN_CODE) {
                let newTop = divMyRocket.offsetTop + ROCKET_STEP; //convertPxToNumber(rocketStyle.top) + ROCKET_STEP;
                newTop = newTop < maxTopPosition ? newTop : maxTopPosition;
                divMyRocket.style.top = `${newTop}px`;
            }
        });
    
        this.compRocket.moveCompRocket();
        this.ball.newSet(BALL_RIGHT);
        this.invertButton(false);
    } 

    fillPlayerCompName() {
        const playerName = opponents[Math.floor(Math.random() * opponents.length)];
        this.divPlayerNameComp.innerText = playerName;
    }

    askPlayerName() {
        let name = prompt("What is your name?", "Cool tennis player");
        while(name === null || name.length < 4) {
            alert("Please enter your name.");
            name = prompt("What is your name?", "Cool tennis player");
        }
        this.divPlayerNameMy.innerText = name;
    }

    invertButton(isStart) {
        if (isStart) {
            this.btnStart.classList.add("btn-start");
            this.btnStart.classList.remove("btn-stop");
            this.btnStart.innerText = "Start";
        } else {
            this.btnStart.classList.remove("btn-start");
            this.btnStart.classList.add("btn-stop");
            this.btnStart.innerText = "Stop";
        }
    }

    checkWinner() {
        if (this.myGoals === WIN_BALL_COUNT || this.compGoals === WIN_BALL_COUNT) {
            this.stopGame();

            const winnerLabel = document.createElement("div");
            winnerLabel.innerText = "Winner";
            winnerLabel.classList.add("label-winner");
            if (this.myGoals === WIN_BALL_COUNT) {
                this.divPlayerNameMy.parentNode.insertBefore(winnerLabel, this.divPlayerNameMy);
            } else {
                this.divPlayerNameComp.parentNode.insertBefore(winnerLabel, this.divPlayerNameComp);
            }
            //we have a winner and save it to score table
            this.saveGame();
            return true;
        }
        //there is no winner, continue game
        return false;
    }

    stopGame() {
        this.invertButton(true);
        this.ball.stop();
        this.compRocket.stopCompRocket();
    }

    saveGame() {
        const newKey = `tennis_res_${window.localStorage.length + 1}`;
        window.localStorage.setItem(newKey, 
            `${this.divPlayerNameComp.innerText} ${this.compGoals} : ${this.myGoals} ${this.divPlayerNameMy.innerText}`);
        console.log(window.localStorage.getItem(newKey));
    }
}

class CompRocket {
    constructor(minTopPosition, maxTopPosition, divCompRocket, ball)  {
        this.minTopPosition = minTopPosition;
        this.maxTopPosition = maxTopPosition;
        
        this.divCompRocket = divCompRocket;
        this.rocketHeight = divCompRocket.offsetHeight; //convertPxToNumber(getComputedStyle(divCompRocket).height);
        this.curTopCompRocket = minTopPosition;
        this.curCompDirectionMove = COMP_MOVE_DOWN;
    
        this.intervalRocket = undefined;

        //Compuet must see a ball to adjust own coordinates
        this.ball = ball;
        this.ballHeigth = ball.divBall.offsetHeight; //convertPxToNumber(this.ballStyle.height);
    }

    moveCompRocket() {
        this.intervalRocket = setInterval(() => {
            if (this.ball.curBallDirection === BALL_RIGHT || this.ball.curBallDirection === BALL_STOP) {
                //if ball is throwing to right, do nothing
                return;
            }

            let topBallPos = this.ball.divBall.offsetTop, //convertPxToNumber(this.ballStyle.top),
                bottomBallPos = topBallPos + this.ballHeigth,
                bottomRocketPos = this.curTopCompRocket + this.rocketHeight;

            if (topBallPos >= this.curTopCompRocket &&
                bottomBallPos <= bottomRocketPos) {
                //if ball infront of the rocket, look at his direction
                if (this.ball.curBallFalling === BALL_DOWN && this.curCompDirectionMove === COMP_MOVE_UP) {
                    this.curCompDirectionMove = COMP_MOVE_DOWN;
                } else if (this.ball.curBallFalling === BALL_UP && this.curCompDirectionMove === COMP_MOVE_DOWN) {
                    this.curCompDirectionMove = COMP_MOVE_UP;
                }
            } else if (topBallPos < this.curTopCompRocket && this.curCompDirectionMove === COMP_MOVE_DOWN) {
                this.curCompDirectionMove = COMP_MOVE_UP;
            } else if (bottomBallPos > bottomRocketPos && this.curCompDirectionMove === COMP_MOVE_UP) {
                this.curCompDirectionMove = COMP_MOVE_DOWN;
            }

            let newTop = this.curTopCompRocket + this.curCompDirectionMove;
            if (newTop < this.minTopPosition) {
                newTop = this.minTopPosition;
                this.curCompDirectionMove = COMP_MOVE_DOWN;
            } else if (newTop > this.maxTopPosition) {
                newTop = this.maxTopPosition;
                this.curCompDirectionMove = COMP_MOVE_UP;
            }
            this.curTopCompRocket = newTop;
            this.divCompRocket.style.top = `${newTop}px`;
        }, 200);
    }

    stopCompRocket() {
        clearInterval(this.intervalRocket);
        this.intervalRocket = undefined;
    }

    isMoving() {
        return this.intervalRocket !== undefined;
    }
}

document.addEventListener("DOMContentLoaded", () => {
    const btnStart = document.querySelector("#btn-start");
    btnStart.classList.add("btn-start");
 
    const game = new Game();
    game.btnStart = btnStart;
    game.invertButton(true);

    btnStart.addEventListener("click", () => {
        game.beginGame();
    });
});

function convertPxToNumber(str) {
    return parseInt(str.substring(0, str.indexOf("px")));
}
