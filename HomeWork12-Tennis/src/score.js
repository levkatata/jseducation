document.addEventListener("DOMContentLoaded", () => {
    const tableBody = document.getElementById("results");
    const results = getResults();

    if (results != null && results.length != 0) {
        tableBody.innerHTML = "";
        for (const r of results) {
            tableBody.appendChild(createRow(r));
        }
    }
});

const regexprResultKey = /^tennis_res_\d{1,}$/;
function getResults() {
    const results = [];

    for (let i = 0; i < localStorage.length; i++) {
        const key = localStorage.key(i);
        if (regexprResultKey.test(key)) {
            results.push(localStorage.getItem(key));
        }
    }

    return results;
}

function createRow(result) {
    const row = document.createElement("tr"),
        data1 = document.createElement("td"),
        data2 = document.createElement("td"),
        dataSemicolon = document.createElement("td");
    const resToShow = result.split(':');

    data1.innerText = resToShow[0];
    data1.classList.add("table-cell-left");
    data2.innerText = resToShow[1];
    data2.classList.add("table-cell-right");
    if (resToShow[0].indexOf('5') != -1) {
        data1.classList.add("table-cell-winner");
    } else if (resToShow[1].indexOf('5') != -1) {
        data2.classList.add("table-cell-winner");
    }
    dataSemicolon.innerText = ":"
    row.appendChild(data1);
    row.appendChild(dataSemicolon);
    row.appendChild(data2);

    return row;
}