/*Створи клас, який буде створювати користувачів з іменем і прізвищем. Додати до класу метода для виводу імені та прізвища.*/

class User {
    constructor(name, surname) {
        this.name = name;
        this.surname = surname;
    }
    showUser() {
        console.log(`User: ${this.name} ${this.surname}`);
    }
}

const user1 = new User("Joe", "Cocker"),
    user2 = new User("David", "Baker"),
    user3 = new User("Princess", "Lua");

user1.showUser();
user2.showUser();
user3.showUser();

/*Створи список, що складається з 4 аркушів. Використовуючи джс зверніться до 2 li та з використанням навігації 
по DOM дай 1 елементу синій фон, а 3 червоний. */

const list2Item = document.querySelector("#list li:nth-child(2)");
list2Item.previousElementSibling.classList.add("blue-background");
list2Item.nextElementSibling.classList.add("red-background");

/*Створи див висотою 400 пікселів і додай на нього подію наведення мишки. Під час наведення мишки виведіть текстом
координати, де знаходиться курсор мишки*/

const mouseEventDiv = document.querySelector(".mouse-event-div");
mouseEventDiv.addEventListener("mousemove", (e) => {
    mouseEventDiv.innerText = `x=${e.offsetX}, y=${e.offsetY}`;
});
mouseEventDiv.addEventListener("mouseleave", () => mouseEventDiv.innerText = "");

/*Створи кнопки 1,2,3,4 і при натисканні на кнопку виведи інформацію про те, яка кнопка була натиснута*/

const [...buttons] = document.querySelectorAll("#button-container > button");
for (let b of buttons) {
    b.addEventListener("click", (e) => { alert(`Button # ${e.target.textContent} is pressed!`) } );
}

/*Створи div і зроби так, щоб при наведенні на div мишкою, div змінював своє положення на сторінці */

const movingDiv = document.getElementById("moving-div");
movingDiv.addEventListener("mouseover", (e) => {
    movingDiv.style.left = `${Math.floor(Math.random() * 800)}px`;
});

/*Створи поле для введення кольору, коли користувач вибере якийсь колір, зроби його фоном body*/

const colorInput = document.getElementById("color-input");
colorInput.addEventListener("input", (e) => {
    document.querySelector("body").style.backgroundColor = colorInput.value;
    return true;
});

/*Створи інпут для введення логіну, коли користувач почне вводити дані в інпут, виводь їх в консоль*/

const loginInput = document.getElementById("login-input");
loginInput.addEventListener("input", (e) => {
    console.log(loginInput.value);
});

/*Створіть поле для введення даних, у полі введення даних виведіть текст під полем */

const textInput = document.getElementById("text-input"), 
    textOutput = document.getElementById("text-output");
textInput.addEventListener("input", (e) => {
    textOutput.innerText = textInput.value;
});
