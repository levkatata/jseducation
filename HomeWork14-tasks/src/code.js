/*1. 
Прив'яжіть усім інпутам наступну подію - втрата фокусу кожен інпут виводить своє value в параграф з id="test"*/

const [...inputs] = document.querySelectorAll("#first-div > input"),
    pTest = document.getElementById("test");
inputs.forEach((i) => {
    i.addEventListener("focusout", (e) => {
        pTest.innerText = e.target.value;
    });
});

/*2.
Дано інпути. Зробіть так, щоб усі інпути втрати фокусу перевіряли свій вміст на правильну кількість символів. 
Скільки символів має бути в інпуті, зазначається в атрибуті data-length. Якщо вбито правильну кількість, 
то межа інпуту стає зеленою, якщо неправильна – червоною.*/

const [...inputsDataLength] = document.querySelectorAll("input[data-length]");
inputsDataLength.forEach((i) => {
    i.addEventListener("focusout", (e) => {
        const v = e.target.value;
        if (v != null && v.length != e.target.getAttribute("data-length")) {
            e.target.classList.remove("right-input");
            e.target.classList.add("wrong-input");
        } else {
            e.target.classList.remove("wrong-input");
            e.target.classList.add("right-input");
        }
    });
});

/*3.
Використовуючи бібліотеку bootstrap створіть форму у якій запитати у користувача данні.
Ім'я, Прізвище (Українською)
Список з містами України 
Номер телефону у форматі +380XX XXX XX XX - Визначити код оператора та підтягувати логотип оператора. 
Пошта 
Якщо поле має помилку показати червоний хрестик  біля поля ❌,  якщо помилки немає показати зелену галочку ✅

Перевіряти данні на етапі втрати фокуса та коли йде натискання кнопки відправити дані 
*/

const regFirstName = /^([A-Za-zА-Яа-я\s]{2,})$/, 
    regPhone = /^\+380[0-9]{9}$/,
    regEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
    request = new XMLHttpRequest(),
    selectCity = document.getElementById("city"),
    PATTERN_ATTR = "check-pattern";

// get the list of city by AXAJ request
request.open("GET", "./ua.json");
request.onreadystatechange = function () {
    if (request.readyState === 4) {
        if(request.status === 200) {
            const cities = JSON.parse(request.responseText);
            for(const c of cities) {
                const option = document.createElement("option");
                option.value = c.city;
                option.textContent = c.city;
                selectCity.appendChild(option);
            }
            //console.log(cities);
        } else {
            console.error("Request was executed with error code " + request.status);
        }
    }
}
request.send();

const [...userInfos] = document.querySelectorAll("#third-div input, #third-div select");
for (const i of userInfos) {
    i.addEventListener("focusout", (e) => {
        if (e.target.id === "last-name" || e.target.id === "first-name") {
            appendSign(regFirstName, e.target);
        } else if (e.target.id === "phone") {
            appendSign(regPhone, e.target);
        } else if (e.target.id === "email") {
            appendSign(regEmail, e.target);
        } else if (e.target.id === "city") {
            appendSign(regFirstName, e.target);
        }
    });
}

function appendSign(regExp, element) {
    const p = document.createElement("span");
    if (element.value.match(regExp) !== null) {
        p.innerText = "✅";
    } else {
        p.innerText = "❌";
    }
    if (element.nextElementSibling !== null) {
        element.nextElementSibling.remove();
    }
    element.after(p);

}

/*4.
При завантаженні сторінки показати користувачеві поле введення (`input`) з написом `Price`. 
Це поле буде служити для введення числових значень. Поведінка поля має бути такою:
- При фокусі на полі введення – у нього має з'явитися рамка зеленого кольору. При втраті фокусу вона пропадає.
- Коли забрали фокус з поля - його значення зчитується, над полем створюється `span`, в якому має бути виведений текст: 
. 
Поруч із ним має бути кнопка з хрестиком (`X`). Значення всередині поля введення фарбується зеленим.
- При натисканні на `Х` - `span` з текстом та кнопка `X` повинні бути видалені.
- Якщо користувач ввів число менше 0 - при втраті фокусу підсвічувати поле введення червоною рамкою, 
під полем виводити фразу - `Please enter correct price`. `span` зі значенням при цьому не створюється. */

const priceInput = document.getElementById("price");

priceInput.addEventListener("focusout", (e) => {
    if (e.target.value > 0) {
        //create field with price and X button
        const divPrice = document.createElement("div"),
            labelPrice = document.createElement("span"),
            btnPrice = document.createElement("button");
        labelPrice.innerText = e.target.value;
        btnPrice.innerText = "x";
        btnPrice.style.margin = '5px';
        divPrice.append(labelPrice, btnPrice);
        btnPrice.addEventListener("click" , (e) => {
            e.target.parentElement.remove();
        });
        //check if there is a field with price, remove it
        if (priceInput.previousElementSibling.previousElementSibling !== null) {
            priceInput.previousElementSibling.previousElementSibling.remove();
        }
        //insert a new field with price and button
        priceInput.previousElementSibling.before(divPrice);
        priceInput.classList.remove("wrong-input");

        //check if there is a field with price error, remove it
        if (priceInput.nextElementSibling !== null) {
            priceInput.nextElementSibling.remove();
        }
    } else {
        if (priceInput.nextElementSibling === null) {
            //create field with price error message
            const spanError = document.createElement("span");
            spanError.innerText = "Please enter correct price";
            spanError.classList.add("error-text");
            //add red error border to the input
            priceInput.classList.add("wrong-input");
            priceInput.after(spanError);
        }
        //check if there is a field with price, remove it if it exists
        if (priceInput.previousElementSibling.previousElementSibling !== null) {
            priceInput.previousElementSibling.previousElementSibling.remove();
        }
    }
})