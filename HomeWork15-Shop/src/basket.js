import { removeItemFromBasket, KEY_BASKET, updateItemCount } from "./module.js";

document.addEventListener("DOMContentLoaded", () => {
    loadBasket();
});

function loadBasket() {
    const basket = JSON.parse(localStorage.getItem(KEY_BASKET));
    //debugger;

    const emptyBasket = document.getElementById("div-empty-basket-text");
    const mainContainer = document.querySelector(".main-basket div:first-child");
    mainContainer.innerHTML = "";

    if (basket !== null && basket.length != 0) {
        for (let p of basket) {
            const divOneProduct = createBasketItem(p);
            mainContainer.append(divOneProduct);
        }
        emptyBasket.classList.remove("visible");
        emptyBasket.classList.add("unvisible");
    } else {
        emptyBasket.classList.add("visible");
        emptyBasket.classList.remove("unvisible");
    }
}

function createBasketItem(product) {
    const divProductCard = document.createElement("div"),
        imgProduct = document.createElement("img"),
        divProductName = document.createElement("div"),
        inputNumber = document.createElement("input"),
        imgDelete = document.createElement("img");

    divProductCard.classList.add("div-basket-product");
    imgProduct.src = product["url"];
    imgProduct.alt = "Product photo";
    divProductName.classList.add("div-product-name");
    divProductName.innerText = product["name"];
    inputNumber.value = product["count"] || 1;
    inputNumber.type = "number";
    imgDelete.src = "../image/delete-forever.png";
    imgDelete.alt = "Delete icon";

    divProductCard.append(imgProduct);
    divProductCard.append(divProductName);
    divProductCard.append(inputNumber);
    divProductCard.append(imgDelete);

    imgDelete.addEventListener("click", (e) => {
        removeItemFromBasket(product);
        loadBasket();
    });
    inputNumber.addEventListener("change", (e) => {
        if (e.target.value <= 0) {
            alert("Wrong count of items was entered! Must be more than 1");
            e.target.value = 1;
        } else {
            updateItemCount(e.target.value, product);
        }
    })

    return divProductCard;
}