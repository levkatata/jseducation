import { initBasketCount, createCard, loadProducts } from "./module.js";

class Filter {
    constructor() {
        this.size = 44; //0 means all sizes
        this.color = "white"; //0 means all colors
        this.divSize = null;
        this.divColor = null;
        this.productsFilteres = [];
        this.allProducts = [];
        this.page = 0; //page 0 means the first page of products, from 0 to 9; 
            //page 1 means the second page of products, from 10 to 19,
    }

    filterProducts(products) {
        this.productsFilteres = products;
        this.page = 0;
        if (this.size !== 0) {
            this.productsFilteres = products.filter(p => p["size"].includes(this.size));
        }
        if (this.color !== 0) {
            this.productsFilteres = this.productsFilteres.filter(p => p["colors"].includes(this.color));
        }
        console.log(this.productsFilteres);
    }

    clearFilters(products) {
        this.size = 0;
        this.page = 0;
        if (this.divSize !== null) {
            this.divSize.classList.remove("div-selected");
        }
        this.color = 0;
        if (this.divColor !== null) {
            this.divColor.classList.remove("div-selected");
        }
        this.productsFilteres = products;
    }
    getPagedProducts() {
        if (this.page * 10 > this.productsFilteres.length) {
            this.page = this.productsFilteres.length / 10;
        }
        return this.productsFilteres.slice(this.page * 10, this.page * 10 + 10);
    }
}

const filter = new Filter();

document.addEventListener("DOMContentLoaded", () => {
    initBasketCount();
    onPageLoaded();
});

function onPageLoaded() {
    const btnApply = document.getElementById("btn-apply"),
        btnClear = document.getElementById("btn-clear"),
        divSizes = document.getElementById("div-sizes"),
        divColors = document.getElementById("div-colors");

    //Initialize filter sizes and color components
    const [...allDivSizes] = document.querySelectorAll("div[id='div-sizes'] > div"),
        [...allDivColors] = document.querySelectorAll("div[id='div-colors'] > div");
    //console.log(allDivSizes, allDivColors);
    for (let div of allDivSizes) {
        div.addEventListener("click", onSizeSelected);
    }
    for (let div of allDivColors) {
        div.addEventListener("click", onColorSelected);
    }

    //Get the list of all products, and in readyProductListListener we will get list of loaded products
    loadProducts("../products.json", readyProductListListener);

    btnApply.addEventListener("click", (e) => {
        filter.filterProducts(filter.allProducts);
        showProducts();
        initializePageNumbers();
    });
    btnClear.addEventListener("click", (e) => {
        filter.clearFilters(filter.allProducts);
        showProducts();
        initializePageNumbers();
    });
    
}

function readyProductListListener(allProducts) {
    console.log(allProducts);
    filter.productsFilteres = allProducts;
    filter.allProducts = allProducts;
    showProducts();
    initializePageNumbers();
}

function initializePageNumbers() {
    //Initialize page number container
    const divPageContainer = document.querySelector("div[class='div-pages']"),
        pagesCount = filter.productsFilteres.length / 10;
    divPageContainer.innerText = "";

    for (let i = 0; i < pagesCount; i++) {
        const divPageNumber = document.createElement("div");
        divPageNumber.innerText = `${i + 1}`;
        divPageNumber.classList.add("div-page-number");
        divPageNumber.addEventListener("click", (e) => {
            //remove previously selected page style
            const divPageSelected = document.querySelector(".div-page-selected");
            if(divPageSelected !== null) {
                divPageSelected.classList.remove("div-page-selected");
            }

            filter.page = parseInt(e.target.innerText) - 1;
            e.target.classList.add("div-page-selected");
            showProducts();
        });
        //adding selection style
        if (i === filter.page) {
            divPageNumber.classList.add("div-page-selected");
        }
        divPageContainer.append(divPageNumber);
    }
}

function showProducts() {
    const displayProducts = filter.getPagedProducts();
    const catalogCards = document.getElementById("catalog-cards");
    catalogCards.innerHTML = "";
    for (const p of displayProducts) {
        const newCard = createCard("../", p);
        catalogCards.append(newCard);
    }
}


function onSizeSelected(e) {
    console.log(e);
    if (filter.divSize !== null) {
        filter.divSize.classList.remove("div-selected");
    }
    filter.size = parseInt(e.target.innerText);
    filter.divSize = e.target;
    e.target.classList.add("div-selected");
}
function onColorSelected(e) {
    console.log(filter.color = e.target.getAttribute("color"));
    if (filter.divColor !== null) {
        filter.divColor.classList.remove("div-selected");
    }
    filter.divColor = e.target;
    e.target.classList.add("div-selected");
}
