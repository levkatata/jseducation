import { initBasketCount, loadProducts, createCard } from "./module.js";

document.addEventListener("DOMContentLoaded", (e) => {
    initBasketCount();
    showProducts();
});

function showProducts() {
    const cards = document.querySelector(".div-cards");
    loadProducts("./products.json", function(products) {
        const beginIndex = Math.floor(Math.random() * (products.length - 4));
        for (let i = beginIndex; i < beginIndex + 4; i++) {
            const divCard = createCard("./", products[i]);
            cards.append(divCard); 
        }
    });
}

