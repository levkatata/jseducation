export const KEY_CURRENT_PRODUCT = "cur_prod",
    KEY_BASKET = "basket";

//Function add item to basket if it is not already in the basket,
// or increment count of items if the item is already in the basket
export function addItemToBasket(product) {
    // debugger;
    const basket = JSON.parse(localStorage.getItem(KEY_BASKET)) || [];

    const existProduct = basket.find(p => p["id"] === product["id"]);
    if (existProduct !== undefined) {
        const count = existProduct["count"] || 1;
        existProduct["count"] = parseInt(count) + 1;
    } else {
        //adding field "count" in product object
        product["count"] = 1;
        basket.push(product);
    }

    localStorage.removeItem(KEY_BASKET);
    localStorage.setItem(KEY_BASKET, JSON.stringify(basket));

    console.log("Our basket is:");
    console.dir(localStorage.getItem(KEY_BASKET));
    alert(`${product['name']} was added to the basket!`);
}

//Function removes the item with the particular product["id"]
export function removeItemFromBasket(product) {
    console.log(localStorage.getItem(KEY_BASKET));
    const basket = JSON.parse(localStorage.getItem(KEY_BASKET)) || [];
    localStorage.removeItem(KEY_BASKET);

    const newBasket = JSON.stringify(basket.filter((p) => {
        if (p["id"] !== product["id"]) {
            return true;
        } else {
            return false;
        }
    }));

    localStorage.setItem(KEY_BASKET, newBasket);
}

export function updateItemCount(newCount, product) {
    if (newCount <= 0) {
        console.error("New count is wrong :", newCount);
        return;
    }

    const currentBasket = JSON.parse(localStorage.getItem(KEY_BASKET));

    for (const p of currentBasket) {
        if (p["id"] === product["id"]) {
            p["count"] = newCount;
        }
    }

    localStorage.setItem(KEY_BASKET, JSON.stringify(currentBasket));
}

export function initBasketCount() {
    const basket = JSON.parse(localStorage.getItem(KEY_BASKET)) || [];
    const spanCount = document.getElementById("span-count");
    if (basket.length != 0) {
        spanCount.innerText = basket.length;
        spanCount.classList.add("visible");
        spanCount.classList.remove("unvisible");
    } else {
        spanCount.classList.add("unvisible");
        spanCount.classList.remove("visible");
    }
}

export function createCard(path, card) {
    const cardDiv = document.createElement("div"),
        img = document.createElement("img"),
        name = document.createElement("div"),
        stars = document.createElement("img"),
        price = document.createElement("div"),
        colors = document.createElement("div"),
        btnAdd = document.createElement("button");

    cardDiv.classList.add("div-card");
    name.classList.add("div-card-name");
    price.classList.add("div-card-price");
    btnAdd.classList.add("btn-card");
    colors.classList.add("div-sizes");
    colors.classList.add("div-card-colors");

    img.src = card["url"];
    name.innerText = card["name"];
    stars.src = `${path}image/stars-all.png`;
    price.innerText = `As low as ${card["price"]}$`;
    btnAdd.innerText = "ADD TO CARD";

    for(const c of card["colors"]) {
        const oneColor = document.createElement("div");
        oneColor.classList.add("div-color");
        oneColor.classList.add(`div-bg-${c}`);
        colors.append(oneColor);
    }

    cardDiv.append(img);
    cardDiv.append(name);
    cardDiv.append(colors);
    cardDiv.append(stars);
    cardDiv.append(price);
    cardDiv.append(btnAdd);

    cardDiv.addEventListener("click", () => {
        localStorage.removeItem(KEY_CURRENT_PRODUCT);
        localStorage.setItem(KEY_CURRENT_PRODUCT, JSON.stringify(card));
        window.location = `${path}product/`;
    });

    btnAdd.addEventListener("click", (e) => {
        e.stopPropagation();
        addItemToBasket(card);
        initBasketCount();
    });

    return cardDiv;
}

export function loadProducts(url, listener) {
    const request = new XMLHttpRequest();
    //Get the list of all products
    request.open("GET", url);
    request.onreadystatechange = (function() {
        if (request.readyState === 4) {
            if (request.status === 200) {
                const allProducts = JSON.parse(request.responseText);
                listener(allProducts);
            } else {
                console.error("Request can't fetch products: " + request.status);
            }
        }    
    });
    request.send();
}