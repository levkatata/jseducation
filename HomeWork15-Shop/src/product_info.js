import {addItemToBasket, KEY_CURRENT_PRODUCT, initBasketCount} from "./module.js";

const product = JSON.parse(localStorage.getItem(KEY_CURRENT_PRODUCT));

document.addEventListener("DOMContentLoaded", () => {
    fillProductDetails();
    initBasketCount();
});

function fillProductDetails() {
    const divProdName = document.getElementById("div-name-prod1"),
        divProdName2 = document.getElementById("div-name-prod2"),
        imgPhoto = document.querySelector(".div-photo > img"),
        divPrice = document.getElementById("div-price"),
        divColors = document.getElementById("div-colors"),
        divSizes = document.getElementById("div-sizes"),
        btnAdd = document.getElementById("btn-add");

    divProdName.innerText = `> ${product["name"]}`;
    divProdName2.innerText = product["name"];
    imgPhoto.src = product["url"];
    divPrice.innerText = `${product["price"]}$`;

    for(const c of product["colors"]) {
        const oneColor = document.createElement("div");
        oneColor.classList.add("div-color");
        oneColor.classList.add(`div-bg-${c}`);
        divColors.append(oneColor);
    }

    for (const s of product["size"]) {
        const oneSize = document.createElement("div");
        oneSize.innerText = s;
        oneSize.classList.add("div-size");
        divSizes.append(oneSize);
    }

    btnAdd.addEventListener("click", (e) => {
        e.stopPropagation();
        addItemToBasket(product);
        initBasketCount();
    })
}