const spans = [];
const KEY_DEALS = "DEALS";

async function req(url) {
    document.querySelector(".loader").classList.add("active")
    const data = await fetch(url);
    return data.json();
}

function show() {
    const display = document.querySelector(".display");
    display.innerHTML = "";
    const data = JSON.parse(localStorage.getItem(KEY_DEALS));
    console.log(data);
    const table = `
        <table>
         <thead>
          <tr>
           <th>
           №
           </th>
           <th>
           Задача
           </th>
           <th>
           Статус
           </th>
           <th>
           Редагувати
           </th>
          </tr>
         </thead>
         <tbody>
           ${data.map((obj, i) => {
        const span = document.createElement("span");
        span.addEventListener("click", (e) => {
            clickEditDeal(obj, e);
        });
        span.innerHTML = "&#128295;"
        spans.push(span);
        return `
               <tr>
                    <td>
                    ${obj.id}
                    </td>
                    <td>
                    ${obj.title}
                    </td>
                    <td>
                    ${obj.completed ? "&#9989;" : "&#10060;"}
                    </td>
                    <td data-span="${i}">
                      
                    </td>
                </tr>`
    }).join("")}
         </tbody>
         </tablr>
        `;
    display.insertAdjacentHTML("beforeend", table);

    document.querySelectorAll("td[data-span]")
        .forEach((e, i) => {
            console.log()
            e.append(spans[i])
        });
    hideLoader();
}

function hideLoader() {
    document.querySelector(".loader").classList.remove("active")
}

function clickEditDeal(obj, e) {
    const status = document.getElementById("status");
    document.querySelector(".parent").classList.add("active");
    document.getElementById("description").innerText = obj.title;
    status.checked = obj.completed;

    document.getElementById("save").onclick = () =>{
        obj.completed = status.checked;
        updateDeals(obj);
        document.querySelector(".parent").classList.remove("active");
        show();
    }
}

function saveDeals(data) {
    localStorage.setItem(KEY_DEALS, JSON.stringify(data));
}

function updateDeals(obj) {
    const deals = JSON.parse(localStorage.getItem(KEY_DEALS));
    const newDeals = deals.map((d) => {
        if (d.id === obj.id) {
            d.completed = obj.completed;
        }
        return d;
    })
    saveDeals(newDeals);
}

function showStarwar(data) {
    const display = document.querySelector(".display");
    console.log(data);

    display.innerHTML = "";
    const table = document.createElement("table"),
        tableHeader = document.createElement("tr"), 
        tableBody = document.createElement("tbody");
    
    tableHeader.insertAdjacentHTML("afterbegin", "<th>Name</th><th>Hair</th><th>Height</th><th>Gender</th><th>Eye color</th>")
    table.append(tableHeader);

    for (const h of data.results) {
        const row = document.createElement("tr");
        row.insertAdjacentHTML("afterbegin", 
            `<td>${h.name}</td><td>${h.hair_color}</td><td>${h.height}</td><td>${h.gender}</td><td>${h.eye_color}</td>`);
            tableBody.append(row);
    }
    table.append(tableBody);

    hideLoader();
    display.append(table);
}

function showMoney(data) {
    const display = document.querySelector(".display");
    console.log(data);

    display.innerHTML = "";
    const table = document.createElement("table"),
        tableHeader = document.createElement("tr"), 
        tableBody = document.createElement("tbody");
    
    tableHeader.insertAdjacentHTML("afterbegin", "<th>Code</th><th>Name</th><th>Rate</th><th>Date</th>")
    table.append(tableHeader);

    for (const m of data) {
        const row = document.createElement("tr");
        row.insertAdjacentHTML("afterbegin", 
            `<td>${m.cc}</td><td>${m.txt}</td><td>${m.rate}</td><td>${m.exchangedate}</td>`);
            tableBody.append(row);
    }
    table.append(tableBody);
     
// cc: "AUD"
// exchangedate: "03.02.2023"
// r030: 36
// rate: 26.0734
// txt: "Австралійський долар"
    hideLoader();
    display.append(table);
}

export { req, show, saveDeals, showStarwar, showMoney };