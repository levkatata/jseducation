import { req, saveDeals, show, showStarwar, showMoney } from "./function.js"
/*
Зробити програму з навігаційним меню яке буде показувати один з варіантів. 
Курс валют НБУ з датою на який день,  https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json
героїв зоряних війн, https://swapi.dev/api/people/
список справ з https://jsonplaceholder.typicode.com/ виводити які виконані які та які ні з можливістю редагування
*/

document.getElementById("starwars").addEventListener("click", () => {
    req("https://swapi.dev/api/people/").then(info => {
        showStarwar(info);
    });
});

document.getElementById("moneyrate").addEventListener("click", () => {
    req("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json").then(info => {
        showMoney(info);
    });
});

document.getElementById("todo").addEventListener("click", () => {
    req("https://jsonplaceholder.typicode.com/todos")
        .then(info => {
            saveDeals(info);
            show();
        });
});

document.querySelector("#close")
    .addEventListener("click", e => document.querySelector(".parent").classList.remove("active"));







