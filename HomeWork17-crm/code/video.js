const KEY_VIDEO = "DBVideo";

//The function is used to create both save and update modal dialogs
export function createVideoModal(categoryInfo, objInfo = null) {
    categoryInfo.insertAdjacentHTML("afterbegin",
        `<div>
            <label for="id-name">Назва відео:</label>
            <input type="text" id="id-name" placeholder="Назва відео">
        </div>
        <div>
            <input type="radio" checked="checked" name="radio" id="id-url">
            <label for="id-url">Url</label>
            <input type="text" id="id-input-url" placeholder="Url to video in youtube.com">
        </div>
        <div>
            <input type="radio" name="radio" id="id-file">
            <label for="id-file">Файл</label>
            <input type="text" readonly id="id-input-file" placeholder="Enter ../videos/bird.mp4">
        </div>
    `);
    const radioUrl = document.getElementById("id-url"),
        radioFile = document.getElementById("id-file"),
        inputUrl = document.getElementById("id-input-url"),
        inputFilepath = document.getElementById("id-input-file");

    radioUrl.addEventListener("change", (e) => {
        console.log(e.target);
        inputUrl.removeAttribute("readonly");
        inputFilepath.setAttribute("readonly", "");
    });
    radioFile.addEventListener("change", (e) => {
        console.log(e.target);
        inputUrl.setAttribute("readonly", "");
        inputFilepath.removeAttribute("readonly");
    });

    if (objInfo !== null) {
        const inputName = document.getElementById("id-name");
        inputName.value = objInfo.videoName;
        inputFilepath.value = objInfo.videoFilepath;
        inputUrl.value = objInfo.videoUrl;

        if (objInfo.videoFilepath.length !== 0) {
            radioFile.setAttribute("checked", "");
            inputFilepath.removeAttribute("readonly", "");
            inputUrl.setAttribute("readonly", "");
        } else {
            radioUrl.setAttribute("checked", "");
            inputUrl.removeAttribute("readonly", "");
            inputFilepath.setAttribute("readonly", "");
        }
    }
}

//This function can both save and update video information
export function saveVideoInfo(objInfo) {
    const inputName = document.getElementById("id-name"),
        radioUrl = document.getElementById("id-url"),
        radioFile = document.getElementById("id-file"),
        inputUrl = document.getElementById("id-input-url"),
        inputFilepath = document.getElementById("id-input-file");
    let canSave = true; // This flag used for validation fields

    inputName.classList.remove("error");
    inputUrl.classList.remove("error");
    inputFilepath.classList.remove("error");

    //Getting database from local storage
    let db = JSON.parse(localStorage.getItem(KEY_VIDEO)) || [];
    
    objInfo.videoName = inputName.value;
    if (objInfo.videoName.length <= 3) {
        inputName.classList.add("error");
        canSave = false;
    } 

    if (radioUrl.checked) {
        objInfo.videoUrl = inputUrl.value;
        objInfo.videoFilepath = "";
        if (objInfo.videoUrl.length <= 3) {
            canSave = false;
            inputUrl.classList.add("error");
        } 
    } else if (radioFile.checked) {
        objInfo.videoFilepath = inputFilepath.value;
        objInfo.videoUrl = "";
        if (objInfo.videoFilepath.length <= 3) {
            canSave = false;
            inputFilepath.classList.add("error");
        } 
    }

    if (canSave) {
        //If validation is ok, try to find video obj id in data base
        //If objInfo doesn't have id, so we must create a new one and save
        if (objInfo.id === null || objInfo.id === "") {
            objInfo.id = db.length === 0 ? 1 : db[db.length - 1].id + 1;
            const date = new Date();
            objInfo.dateAdded = `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`;
        
            db.push(objInfo);
            localStorage.setItem(KEY_VIDEO, JSON.stringify(db));
        
            inputName.value = "";
            inputUrl.value = "";
            inputFilepath.value = "";
        } else {
            //If we have found the item in DB, so just update it.
            for (let i = 0; i < db.length; i++) {
                if (db[i].id === objInfo.id) {
                    db[i].videoName = objInfo.videoName;
                    db[i].videoFilepath = objInfo.videoFilepath;
                    db[i].videoUrl = objInfo.videoUrl;
                }
            }
            localStorage.setItem(KEY_VIDEO, JSON.stringify(db));
            showVideo();
        }
    } else {
        console.error("Unable to save video!");
    }
}

document.addEventListener("DOMContentLoaded", () => {
    if (window.location.pathname.includes("video")) {
        showVideo();
    }
});

//Function creates modal dialog window for editing video information
function editVideo(objInfo) {
    debugger;
    const modal = document.querySelector(".container-modal"),
        modalContent = document.querySelector(".category-info");
    modal.classList.add("active");
    modalContent.innerHTML = "";
    //We are using the same function to create modal dialog as in the main screen
    createVideoModal(modalContent, objInfo);

    document.getElementById("save").onclick = (e) => {
        saveVideoInfo(objInfo);
        modal.classList.remove("active");
    };
    document.getElementById("close").onclick = (e) => {
        modal.classList.remove("active");
    };
}

//This function is used to fill up the table with video information
function showVideo() {
    const tableBody = document.getElementById("video-table");
    if (tableBody !== null) {
        const data = JSON.parse(localStorage.getItem(KEY_VIDEO));
        tableBody.innerHTML = "";
    
        for (const d of data) {
            const row = document.createElement("tr"),
                spanEdit = document.createElement("span"),
                tdEdit = document.createElement("td"),
                tdDelete = document.createElement("td"),
                spanDelete = document.createElement("span");
    
            const videoRef = d.videoFilepath !== "" ? d.videoFilepath : d.videoUrl;
            row.insertAdjacentHTML("afterbegin", 
                `<td>${d.id}</td><td>${d.videoName}</td><td>${d.dateAdded}</td>
                    <td><a href=${videoRef} target="_blank">${videoRef}</a></td>`);
            
            //Adding a span element with edit symbol
            spanEdit.innerHTML = "&#9998;";
            tdEdit.append(spanEdit);
            row.append(tdEdit);
            spanEdit.addEventListener("click", () => {
                editVideo(d);
            });
    
            spanDelete.innerHTML = "&#129530;";
            tdDelete.append(spanDelete);
            row.append(tdDelete);
            spanDelete.addEventListener("click", () => {
                deleteVideo(d.id);
            });
    
            tableBody.append(row);
        }
    }
}

function deleteVideo(id) {
    const db = JSON.parse(localStorage.getItem(KEY_VIDEO));
    const newDb = db.filter(item => item.id != id);
    localStorage.setItem(KEY_VIDEO, JSON.stringify(newDb));
    showVideo();
}