import {Hamburger} from './hamburger.js';

document.addEventListener("DOMContentLoaded", () => {
    const p1 = document.getElementById("ham-info1"),
        p2 = document.getElementById("ham-info2"),
        p3 = document.getElementById("ham-info3");

    const h1 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_POTATO),
        h2 = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_CHEESE);

    h1.addTopping(Hamburger.TOPPING_SPICE);
    h2.addTopping(Hamburger.TOPPING_MAYO);

    p1.innerText = `${h1}, price=${h1.calculatePrice()}, calories=${h1.calculateCalories()}`;
    p2.innerText = `${h2}, price=${h2.calculatePrice()}, calories=${h2.calculateCalories()}`;

    h2.removeTopping(Hamburger.TOPPING_MAYO);
    p3.innerText = `After removing topping: ${h2}, price=${h2.calculatePrice()}, calories=${h2.calculateCalories()}`;
});