/*Розміри, види начинок та добавок */
export const SIZE_SMALL = "small",
    SIZE_LARGE = "large",
    STUFFING_CHEESE = "cheese",
    STUFFING_SALAD = "salad",
    STUFFING_POTATO = "potato",
    TOPPING_MAYO = "mayo",
    TOPPING_SPICE = "spice";

