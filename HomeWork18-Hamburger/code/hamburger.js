import {SIZE_SMALL, SIZE_LARGE, STUFFING_CHEESE, STUFFING_POTATO, STUFFING_SALAD, TOPPING_MAYO, TOPPING_SPICE} from './constants.js';

/**
Клас, об'єкти якого описують параметри гамбургера.
‌
@constructor
@param size Розмір
@param stuffing Начинка
@throws {HamburgerException} При неправильному використанні*/

class Hamburger {
    static get SIZE_SMALL() { return SIZE_SMALL; }
    static get SIZE_LARGE() { return SIZE_LARGE; }
    static get STUFFING_CHEESE() { return STUFFING_CHEESE; }
    static get STUFFING_POTATO() { return STUFFING_POTATO; }
    static get STUFFING_SALAD() { return STUFFING_SALAD; }
    static get TOPPING_MAYO() { return TOPPING_MAYO; }
    static get TOPPING_SPICE() { return TOPPING_SPICE; }

    constructor(size, stuffing) {
        if (size !== Hamburger.SIZE_LARGE && size !== Hamburger.SIZE_SMALL) {
            throw new HamburgerException("Invalid size: " + size);
        } 
        this.size = size;

        if (stuffing !== Hamburger.STUFFING_CHEESE &&
            stuffing !== Hamburger.STUFFING_POTATO &&
            stuffing !== Hamburger.STUFFING_SALAD) {
            throw new HamburgerException("Invalid stuffing: " + stuffing);
        }
        this.stuffing = stuffing;

        this.toppings = [];
    }
    
    /**
    Додати добавку до гамбургера. Можна додати кілька
    Добавок, за умови, що вони різні.
    ‌
    @param topping Тип добавки
    @throws {HamburgerException} При неправильному використанні
    */
    addTopping(topping) {
        switch(topping) {
            case Hamburger.TOPPING_MAYO:
            case Hamburger.TOPPING_SPICE:
                this.toppings.push(topping);
                break;
            default:
                throw new HamburgerException("Invalid topping: " + topping);
        }
    }

    /*
    Прибрати добавку, за умови, що вона раніше була
    Додано.
    ‌
    @param topping Тип добавки
    @throws {HamburgerException} При неправильному використанні*/
    removeTopping(topping) {
        switch(topping) {
            case Hamburger.TOPPING_MAYO:
            case Hamburger.TOPPING_SPICE:
                this.toppings = this.toppings.filter((t) => {
                    return t !== topping;
                });
                break;
            default:
                throw new HamburgerException("Invalid topping: " + topping);
        }
    }

    /*
    Отримати список добавок.
    @return {Array} Масив доданих добавок містить константи
    Hamburger.TOPPING_ */
    getToppings() {
        return this.toppings.copyWithin(this.toppings.length, 0);
    }
    
    /**
    Дізнатися розмір гамбургера
    */
    getSize() {
        return this.size;
    }

    /*
    Дізнатися начинку гамбургера
    */
    getStuffing() {
        return this.stuffing;
    }
    /*
    Дізнатись ціну гамбургера
    @return {Number} Ціна у тугриках
    */
    calculatePrice() {
        let price = 0;
        if (this.size === Hamburger.SIZE_SMALL) {
            price += 50;
        } else if (this.size === Hamburger.SIZE_LARGE) {
            price += 100;
        } else {
            console.error("Invalid size " + this.size);
        }

        switch(this.stuffing) {
            case Hamburger.STUFFING_CHEESE:
                price += 10;
                break;
            case Hamburger.STUFFING_POTATO:
                price += 15;
                break;
            case Hamburger.STUFFING_SALAD:
                price += 20;
                break;
            default:
                console.error("Invalid stuffing " + this.stuffing);
                break;
        }

        this.toppings.forEach((t) => {
            if (t === Hamburger.TOPPING_MAYO) {
                price += 20;
            } else if (t === Hamburger.TOPPING_SPICE) {
                price += 15;
            }
        });

        return price;
    }
    /*
    Дізнатися калорійність
    @return {Number} Калорійність калорій
    */
    calculateCalories() {
        let calories = 0;
        if (this.size === Hamburger.SIZE_SMALL) {
            calories += 20;
        } else if (this.size === Hamburger.SIZE_LARGE) {
            calories += 40;
        } else {
            console.error("Invalid size " + this.size);
        }

        switch(this.stuffing) {
            case Hamburger.STUFFING_CHEESE:
                calories += 20;
                break;
            case Hamburger.STUFFING_SALAD:
                calories += 5;
                break;
            case Hamburger.STUFFING_POTATO:
                calories += 10;
                break;
            default:
                console.error("Invalid stuffing " + this.stuffing);
        }

        this.toppings.forEach((t) => {
            if (t === Hamburger.TOPPING_MAYO) {
                calories += 5;
            }
        });

        return calories;
    }

    toString() {
        return `Hamburger info: size ${this.getSize()}, stuffing ${this.getStuffing()}, toppings ${this.getToppings()}`;
    }

}

/*Надає інформацію про помилку під час роботи з гамбургером.
Подробиці зберігаються у властивості message.
@constructor
*/
class HamburgerException { 
    constructor(msg) {
        this.msg = msg;
    }
    toString() {
        return this.msg;
    }
}

const ham1 = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_SALAD);
ham1.addTopping(Hamburger.TOPPING_MAYO);
console.log(ham1.calculatePrice());
console.log(ham1.calculateCalories());
ham1.addTopping(Hamburger.TOPPING_MAYO);
ham1.addTopping(Hamburger.TOPPING_SPICE);
console.log(ham1.calculatePrice());
console.log(ham1.calculateCalories());
console.log(ham1.getToppings());
ham1.removeTopping(Hamburger.TOPPING_MAYO);
console.log(ham1.calculatePrice());
console.log(ham1.calculateCalories());
console.log(ham1.getToppings());

export {Hamburger, HamburgerException};