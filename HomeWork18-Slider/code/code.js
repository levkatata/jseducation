//Information about prices will store in following constants:

const sauceToppingPrices = [{name: "sauceClassic", price: 10}, 
    {name: "sauceBBQ", price: 15}, 
    {name: "sauceRikotta", price: 5},
    {name: "moc1", price: 5}, 
    {name: "moc2", price: 6}, 
    {name: "moc3", price: 7},
    {name: "telya", price: 25},
    {name: "vetch1", price: 9},
    {name: "vetch2", price: 10}];
const sizePrices = [{name: "small", price: 40},
    {name: "mid", price: 50},
    {name: "big", price: 70}];

const errorMsgName = "Помилка при введенні імені!",
    errorMsgPhone = "Помилка при введенні номеру телефону!",
    errorMsgEmail = "Помилка при введенні адреси електроної пошти!",

    patternName = /^([A-Za-zА-Яа-я\s]{2,})$/,
    patternTel = /^\+380[0-9]{9}$/,
    patternEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,

    addressMsg = `© 2002–${new Date().getFullYear()} Мережа піцерій Domino!`;

const lsKeyName = "pizza_username",
    lsKeyPhone = "pizza_phone",
    lsKeyEmail = "pizza_email",
    lsKeyPizzaSize = "pizza_size",
    lsKeySauces = "pizza_sauces",
    lsKeyToppings = "pizza_toppings";        

//Class Ingredient store the information about sauce or topping
class Ingredient {
    #id;
    #name;
    #price;
    #div;

    constructor(name, id, div) {
        this.#name = name;
        this.#price = this.getPriceById(id);
        this.#id = id;
        this.#div = div;
    }

    getPriceById(name) {
        const element = sauceToppingPrices.find((elem) => {
            return elem.name === name;
        });
        return element.price;
    }

    getPrice() {
        return this.#price;
    }

    removeLayout() {
        this.#div.remove();
    }

    getId() {
        return this.#id;
    }
}

//class Pizza store all information about the sauces, toppings and size for pizza which user creates. Calculates the price.
class Pizza {
    #sauces = [];
    #toppings = [];
    #size = sizePrices[2].name;
    #pPrice = undefined;

    constructor(pPrice) {
        this.#pPrice = pPrice;
    }

    addSauce(name, id, div) {
        this.#sauces.push(new Ingredient(name, id, div));
    }

    addTopping(name, id, div) {
        this.#toppings.push(new Ingredient(name, id, div));
    }

    changeSize(size) {
        if (sizePrices.find((e) => e.name === size) === undefined) {
            console.error("Incorrect size " + size);
            return;
        }
        this.#size = size;
    }

    getPriceOfSize() {
        return sizePrices.find((e) => {
            return e.name === this.#size;
        }).price;
    }

    calculateWholePizza() {
        let result = this.getPriceOfSize();
        if (this.#sauces.length !== 0) {
            result += this.#sauces.reduce(function (total, s) {
                return total + s.getPrice();
            }, 0);
        }
        if (this.#toppings.length !== 0) {
            result += this.#toppings.reduce(function (total, t) {
                return total + t.getPrice();
            }, 0);
        }
        return result;
    }

    showPrice() {
        this.#pPrice.innerText = `Ціна: ${this.calculateWholePizza()} грн.`;
    }

    //method removes an ingredient both from the list and layout
    removeIng(id) {
        let ingToRemove = this.#sauces.find((s) => {return s.getId() === id});
        if (ingToRemove !== undefined) {
            const i = this.#sauces.indexOf(ingToRemove);
            this.#sauces.splice(i, 1);
            ingToRemove.removeLayout();
            return;
        }

        ingToRemove = this.#toppings.find((t) => {return t.getId() === id});
        if (ingToRemove !== undefined) {
            const i = this.#toppings.indexOf(ingToRemove);
            this.#toppings.splice(i, 1);
            ingToRemove.removeLayout();
            return;
        }
    }

    getZIndex() {
        return this.#sauces.length + this.#toppings.length + 10;
    }

    
    savePizzaInfo(userName, phone, email) {
        try {
            window.localStorage.setItem(lsKeyName, userName);
            window.localStorage.setItem(lsKeyPhone, phone);
            window.localStorage.setItem(lsKeyEmail, email);
            window.localStorage.setItem(lsKeyPizzaSize, this.#size);
            window.localStorage.setItem(lsKeySauces, JSON.stringify(this.#sauces.map(e => e.getId())));
            window.localStorage.setItem(lsKeyToppings, JSON.stringify(this.#toppings.map(e => e.getId())));
        } catch (e) {
            console.error("Unable to save pizza information");
        }
        console.log(JSON.parse(window.localStorage.getItem(lsKeySauces)));
        console.log(JSON.parse(window.localStorage.getItem(lsKeyToppings)));
    }
}

//This function create div element with the name of ingredient for the list. This div will also contain X button to remove it.
function createIngredient(name, id, pizza) {
    const div = document.createElement("div");
    const divName = document.createElement("div");
    div.classList.add("ingridient");
    divName.innerText = name;
    const button = document.createElement("button");
    button.addEventListener("click", function() {
        this.parentNode.remove();
        pizza.removeIng(id);
        pizza.showPrice();
    });
    button.textContent = "X";
    div.appendChild(divName);
    div.appendChild(button);
    return div;
}

//Function validates input data about user information
function validateInput(div, pattern, errorMsg) {
    if(!div.value.match(pattern)) {
        alert(errorMsg);
        div.classList.add("error");
        div.classList.remove("success");
        return false;
    } else {
        div.classList.remove("error");
        div.classList.add("success");
        return true;
    }
}


window.addEventListener("DOMContentLoaded", () => {
    const pizzaImg = document.getElementById("img-pizza-basic"),
        divIngredients = document.querySelector(".ingridients"),
        [...imgIngredients] = document.querySelectorAll("div.ingridients img"),
        divTable = document.querySelector(".table"),
        pPrice = document.querySelector("div.price p"),
        pSauces = document.querySelector("div.sauces p"),
        pToppings = document.querySelector("div.topings p"),
        banner = document.getElementById("banner"),
        inputName = document.getElementById("name"),
        inputPhone = document.getElementById("phone"),
        inputEmail = document.getElementById("email"),
        btnSubmit = document.getElementById("btnSubmit");

    document.querySelector("address").innerText = addressMsg;

    //collect all ids of sauses and toppings, than we'll use this array in handling events 
    const imgIngredientsIds = imgIngredients.map((i) => {
        console.dir(i.id);
        return {id : i.id, alt : i.alt};
    });

    const pizza = new Pizza(pPrice);
    pizza.showPrice();

    divTable.addEventListener("dragover", (e) => {
        //by default drag is not allowed so we cancel this
        if(e.preventDefault) e.preventDefault();
        return false;
    });

    divTable.addEventListener("drop", function (e) {
        if (e.preventDefault) e.preventDefault();
        if (e.stopPropagation) e.stopPropagation();
        //dataTranfer contains information about ingredient to add: url, id, alt(name of ingredient) 
        if (e.dataTransfer.getData("url") !== "") {
            //create img element to add it to main pizza image
            const imgOneIng = document.createElement("img");
            imgOneIng.src = `${e.dataTransfer.getData("url")}`;
            imgOneIng.style.position = "absolute";
            imgOneIng.style.left = `0px`;
            imgOneIng.style.top = `0px`;
            imgOneIng.style.zIndex = pizza.getZIndex();
            divTable.append(imgOneIng);

            const idIng = e.dataTransfer.getData("id");
            //check wether it is sauce or topping
            if ((/^sauce/g).test(idIng)) {
                const name = e.dataTransfer.getData("alt");
                pizza.addSauce(name, idIng, imgOneIng);
                pSauces.appendChild(createIngredient(name, idIng, pizza));
            } else if (idIng !== "") {
                const name = e.dataTransfer.getData("alt");
                pizza.addTopping(name, idIng, imgOneIng);
                pToppings.appendChild(createIngredient(name, idIng, pizza));
            }
            console.log("Pizza costs: " + pizza.calculateWholePizza());
            pizza.showPrice();
        }
        return false;
    }, true);

    divIngredients.addEventListener("dragstart", function (e) {
        console.dir(e);
        e.dataTransfer.effectAllowed = "move";
        if (e.target && e.target.id 
                //make sure the target contains rigth id of sauces or toppings
                && imgIngredientsIds.find((ing) => {return ing.id === e.target.id}) !== undefined) {
            console.dir("url:" + e.target.src + e.layerX + ' ' + e.layerY);
            e.dataTransfer.setData("url", e.target.src);
            e.dataTransfer.setData("height", e.target.height);
            e.dataTransfer.setData("width", e.target.width);
            e.dataTransfer.setData("id", e.target.id);
            e.dataTransfer.setData("alt", e.target.alt);
        }
    });

    const [...radioPizzaSize] = document.querySelectorAll("input[type=radio]");
    radioPizzaSize.forEach((elem) => {
        elem.addEventListener("change", function(e) {
            //id is the name of pizza size
            pizza.changeSize(e.target.id);
            pizza.showPrice();
        });
    });

    banner.addEventListener("mouseenter", function(e) {
        const posX = e.target.offsetLeft, 
            bannerWidth = e.target.offsetWidth, 
            width = window.innerWidth; 

        //generate a new left coordinate for banner to "run" banner from mouse cursor
        let newPosX;
        if (posX - bannerWidth > width - posX - bannerWidth * 2 - 15) {
            newPosX = Math.floor(Math.random() * (posX - bannerWidth));
        } else {
            newPosX = Math.floor(Math.random() * (width - posX - bannerWidth * 2)) + posX + bannerWidth + 15;
        }

        banner.style.left = `${newPosX}px`;
        banner.style.right = 'auto';
    });

    inputName.addEventListener("change", function(e) {
        console.log(this.value);
        validateInput(inputName, patternName, errorMsgName);
    });
    inputPhone.addEventListener("change", function(e) {
        console.log(this.value);
        validateInput(inputPhone, patternTel, errorMsgPhone);
    });
    inputEmail.addEventListener("change", function(e) {
       validateInput(inputEmail, patternEmail, errorMsgEmail);
    });

    btnSubmit.addEventListener("click", function(e) {
        let result = true;
        result = validateInput(inputName, patternName, errorMsgName);
        result = validateInput(inputPhone, patternTel, errorMsgPhone);
        result = validateInput(inputEmail, patternEmail, errorMsgEmail);
        if (result) {
            //save to local storage or send to server
            pizza.savePizzaInfo(inputName.value, inputPhone.value, inputEmail.value);
        }
    });

    $('.carousel').carousel({interval: 3000});
});
