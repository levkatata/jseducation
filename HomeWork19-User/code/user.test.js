//To run tests, enter in terminal:  npm run test

const User = require('./user');

let user1;


describe("Check User class", () => {

    beforeAll(() => {
        user1 = new User(10, 'levkatata', 'levkatata@gmail.com', 'pass');
    })

    test('Validate ID', () => {
        expect(user1.getId()).toBe(10);
    });
    test('Validate Name', () => {
        expect(user1.getName()).toEqual('levkatata');
    });
    test('Validate Email', () => {
        expect(user1.getEmail()).toEqual('levkatata@gmail.com');
    });
    test('Validate Password', () => {
        expect(user1.getPassword()).toEqual('pass');
    });
        
    test('Validate setting email', () => {
        user1.setEmail('newmail@gmail.com');
        expect(user1.getEmail()).toEqual('newmail@gmail.com');
    });

    test('Validate setting password', () => {
        user1.setPassword('newpass');
        expect(user1.getPassword()).toEqual('newpass');
    });

});