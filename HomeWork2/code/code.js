let space = "&nbsp;&nbsp;", star = "*";


/*Triangle*/
const tRows = 10;

for (let i = 1, s = 1; i <= tRows; i++, s += 2) {
    for(let j = 1; j <= tRows - i; j++) {
        document.write(space);
    }
    for(let j = 1; j <= s; j++) {
        document.write(star);
    }
    document.write("<br/>");
}

document.write("<br/><br/>");

/*Diamond*/

const dRows = 7;
let s = 1;
for (let i = 1; i <= dRows; i++, s += 2) {
    for(let j = 1; j <= dRows - i; j++) {
        document.write(space);
    }
    for(let j = 1; j <= s; j++) {
        document.write(star);
    }
    document.write("<br/>");
} 
s -= 4;
for (let i = 1; i <= dRows; i++, s -= 2) {
    for (let j = 1; j <= i; j++) {
        document.write(space);
    }
    for (let j = 1; j <= s; j++) {
        document.write(star);
    }
    document.write("<br/>");
}

/*Empty rectangle*/

const rRows = 7, rColumns = 20;
let fillChar = "*";

for(let i = 1; i <= rRows; i++) {
    if (i === 1 || i === rRows) {
        fillChar = "*";
    } else {
        fillChar = "&nbsp;&nbsp;"
    }

    for(let j = 1; j <= rColumns; j++) {
        if (j === 1 || j === rColumns) {
            document.write("*");
        } else {
            document.write(fillChar);
        }
    }
    document.write("<br/>");
}

/*Numbers divided by 5*/

let custStrNumber = null;
let custNumber = null;

do {
    custStrNumber = prompt("Enter a positive integer:");
    custNumber = Number.parseFloat(custStrNumber);
    if (Number.isInteger(custNumber) == false || custNumber <= 0) {
        alert("Incorrect number! Please, enter a positive integer.");
    }
    else {
        break;
    }
} while (true);

if (custNumber < 5) {
    console.log("Sorry, no numbers!");
} else {
    console.log("Numbers divisible by 5:");
    for (let i = 0; i <= custNumber; i++) {
        if (i % 5 == 0) {
            console.log(`${i} `);
        }
    }
}