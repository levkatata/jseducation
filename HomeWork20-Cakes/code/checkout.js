import { getCupcakes, displayCupcakeCount, reduceTotal } from "./trolley.js";

const cupcakes = getCupcakes();

document.addEventListener("DOMContentLoaded", ()=> {
    const divCards = document.getElementById("div-checkout-cakes");

    cupcakes.forEach(element => {
        divCards.append(createCardRow(element));
    });
    displayCupcakeCount();
    showTotal();
});

function createCardRow(cake) {
    const divRow = document.createElement("div"),
        imgCake = document.createElement("img"),
        divName = document.createElement("div"),
        inputCount = document.createElement("input"),
        divPrice = document.createElement("div");

    divRow.classList.add("div-row-cake");
    imgCake.src = `.${cake.image}`;
    divName.innerText = cake.name;
    divName.classList.add("text-cake");
    divPrice.classList.add("text-cake");
    divPrice.innerText = `$${(cake.price * cake.count).toFixed(2)}`;
    inputCount.classList.add("input-number");
    inputCount.type = "number";
    inputCount.min = "1";
    inputCount.value = cake.count;
    inputCount.addEventListener("change", (e) => {
        divPrice.innerText = `$${(cake.price * e.target.value).toFixed(2)}`;
        cake.count = e.target.value;
        showTotal();
    });

    divRow.append(imgCake, divName, inputCount, divPrice);

    return divRow;
}

function showTotal() {
    document.getElementById("div-total").innerText = `Total to pay   $${reduceTotal(cupcakes)}`;
}