export const KEY_BAG = "MISS_CUPCAKE_BAG";

document.addEventListener("DOMContentLoaded", () => {
    const divMainMenu = document.getElementById("main-menu"),
        divBurgerMenu = document.getElementById("div-burger-menu");
        
    divBurgerMenu.addEventListener("click", (e) => {
        const displayOption = divMainMenu.classList.contains("div-main-menu-visible");
        console.log(displayOption);
        if (!displayOption) {
            divMainMenu.classList.add("div-main-menu-visible");
        } else {
            divMainMenu.classList.remove("div-main-menu-visible");
        }
    });
});

