import { KEY_BAG } from "./code.js";
import { displayCupcakeCount } from "./trolley.js";

class Cake {
    constructor(id, name, desc, image, price) {
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.image = image;
        this.price = price;
    }
}

const allAnimations = ["flash", "bounce", "pulse"];

async function req(url) {
    const data = await fetch(url);
    return data.json();
}

function createCakeCard(cake) {
    const card = document.createElement("div"),
        img = document.createElement("img"),
        name = document.createElement("div"),
        desc = document.createElement("div"),
        container = document.createElement("div"),
        number = document.createElement("input"),
        btn = document.createElement("button"),
        divImg = document.createElement("div");
    
    divImg.append(img);
    img.src = cake.image;
    img.alt = "Cake img";
    divImg.classList.add("wow", getRandomAnimation());
    name.innerText = cake.name;
    name.classList.add("text-cake-name");
    desc.innerText = cake.desc;
    desc.classList.add("text-purple-small", "text-cake-desc");
    
    number.value = 0;
    number.classList.add("input-number");
    number.type = "number";
    number.min = "0";
    btn.innerText = "Add to card";
    btn.classList.add("btn-purple", "btn-add");
    btn.addEventListener("click", () => {
        addToCard(cake, parseInt(number.value));
    });
    container.classList.add("div-card-button-container");
    container.append(number, btn);

    card.classList.add("div-cake-card");
    card.append(divImg, name, desc, container);

    return card;
}

document.addEventListener("DOMContentLoaded", () => {
    const divCakes = document.getElementById("cakes-container");

    req("./cakes.json").then(([...res]) => {
        const cakes = res.map(element => {
            return new Cake(element.id, element.name, element.desc, element.img, element.price);
        });
        for(const c of cakes) {
            divCakes.append(createCakeCard(c));
        }
    });
});

function addToCard(cake, count = 1) {
    if (count >= 1) {
        const [...trolley] = JSON.parse(localStorage.getItem(KEY_BAG)) || [];
        const cupcakeExist = trolley.find(e => e.id === cake.id);
        if (cupcakeExist !== undefined) {
            cupcakeExist.count = parseInt(cupcakeExist.count) + count;
        } else {
            cake.count = count;
            trolley.push(cake)
        }
        localStorage.setItem(KEY_BAG, JSON.stringify(trolley));

        displayCupcakeCount();
    }
}

function getRandomAnimation() {
    return allAnimations[Math.floor(Math.random() * allAnimations.length)];
}