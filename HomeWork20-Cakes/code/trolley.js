import { KEY_BAG } from "./code.js";

document.addEventListener("DOMContentLoaded", () => {
    const divTrolleyImg = document.getElementById("div-trolley"), 
        divTrolley = document.getElementById("div-trolley-container"), 
        btnCardView = document.getElementById("btn-view-cart"),
        imgCross = document.getElementById("img-cross"),
        btnCheckout = document.getElementById("btn-checkout");

    divTrolleyImg.addEventListener("click", () => {
        const displayOption = divTrolley.classList.contains("div-main-menu-visible");
        console.log(displayOption);
        if (!displayOption) {
            divTrolley.classList.add("div-main-menu-visible");
            
            const cakes = getCupcakes();
            showCupcakes(cakes);
            document.getElementById("div-trolley-total").innerText = 
                `$${reduceTotal(cakes)}`;
        } else {
            divTrolley.classList.remove("div-main-menu-visible");
        }
    });
    
    displayCupcakeCount();

    btnCardView.addEventListener("click", openCheckout);
    btnCheckout.addEventListener("click", openCheckout);

    imgCross.addEventListener("click", () => {
        divTrolley.classList.remove("div-main-menu-visible");
    });
});

function showCupcakes(trolley) {
    const divTrolleyContainer = document.getElementById("trolley-container"),
        divTrolleyEmpty = document.getElementById("trolley-empty");

    if (trolley.length !== 0) {
        divTrolleyEmpty.classList.add("div-empty-hide");
        divTrolleyContainer.innerHTML = "";
        [...trolley].forEach(element => {
            divTrolleyContainer.append(createCupcakeRow(element));
        });
    } else {
        divTrolleyEmpty.classList.remove("div-empty-hide");
        divTrolleyContainer.innerText = "";
    }
}

export function getCupcakes() {
    return JSON.parse(localStorage.getItem(KEY_BAG)) || [];
}
function createCupcakeRow(cake) {
    const divRowContainer = document.createElement("div"),
        imgCake = document.createElement("img"),
        divCakeName = document.createElement("div"),
        divPrice = document.createElement("div");
    divCakeName.innerText = `${cake.name} x ${cake.count}`;
    divPrice.innerText = `$${(parseFloat(cake.price) * parseInt(cake.count)).toFixed(2)}`;
    divRowContainer.classList.add("div-cake-row");
    divRowContainer.append(imgCake, divCakeName, divPrice);

    imgCake.classList.add("img-cake-icon");
    //correct image url depending on the page where trolley has been called
    if(document.location.pathname.includes("why") || document.location.pathname.includes("checkout")) {
        imgCake.src = `.${cake.image}`;
    } else {
        imgCake.src = cake.image;
    }
    return divRowContainer;
}

function getCupcakesCount() {
    return getCupcakes().reduce((accum, curCake) => accum + curCake.count, 0);
}

export function displayCupcakeCount() {
    const cupCakeCount = getCupcakesCount(),
        divTrolleyCount = document.getElementById("div-trolley-count")

    if (cupCakeCount == 0) {
        divTrolleyCount.classList.add("div-empty-hide");
    } else {
        divTrolleyCount.innerText = cupCakeCount;
        divTrolleyCount.classList.remove("div-empty-hide");
    }
}

function openCheckout() {
    if (getCupcakesCount() != 0) {
        if (document.location.pathname.includes("why")) {
            document.location = "../checkout";
        } else {
            document.location = "./checkout/";
        }
    }
}

export function reduceTotal(cakes) {
    const res = cakes.reduce((accum, e) => {
        return parseFloat(accum) + (e.price * e.count);
    }, 0);
    return res.toFixed(2);
}