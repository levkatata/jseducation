const textarea = document.querySelector(".selector");
const teg = document.querySelector(".teg");
const regDot = /[\.]{1}/;


textarea.addEventListener("input", (e) => {
  let text = e.target.value;
  let newTeg = null;

  if (text !== "") {
    const tags = text.split(">");
    newTeg = proccessTag(tags);
    console.log("Log " + newTeg.outerHTML);
  }

  showTeg(newTeg)
});

function createTeg(tegName, className, text) {
  let teg = document.createElement(tegName);
  if (className !== undefined && className !== "") {
    teg.classList.add(className);
  }
  if (text !== undefined && text !== "") {
    teg.innerText = text;
  }

  return teg;
}

function showTeg(obj) {
  teg.innerText = obj ? obj.outerHTML : "";
}

function proccessTag(tags) {
  let text = tags.shift();
  let newTeg = null;
  // перевірка на class
  if(regDot.test(text)){
    let arr = text.split(regDot);
    if(arr[0]===''){
        newTeg =  createTeg('div', extractClass(arr[1]), extractText(arr[1]));
    }else{
        newTeg = createTeg(arr[0], extractClass(arr[1]), extractText(arr[1]));
    }
  }else{
    newTeg = createTeg(extractTag(text), "", extractText(text));
  }

  if (tags.length === 0 || tags[0].length === 0) {
    console.dir(newTeg);
    return newTeg;
  } else {
    console.dir(newTeg);
    newTeg.append(proccessTag(tags));
    return newTeg;
  }

}

function extractText(value) {
  let posOpen = value.indexOf("{"), 
    posClose = value.indexOf("}");

  if ((posOpen == -1 && posClose == -1) ||
    (posOpen > posClose)) {
    return "";
  }

  return value.substring(posOpen + 1, posClose);
}

function extractClass(value) {
  let pos = value.indexOf('{');
  if (pos != -1) {
    return value.substring(0, pos);
  } 
  pos = value.indexOf(">");
  if (pos != -1) {
    return value.substring(0, pos);
  }
    
  return value;
}

function extractTag(value) {
  let posOpen = value.indexOf("{"),
    posDot = value.indexOf(".");

  if (posDot !== -1) {
    return value.substring(0, posDot);
  }
  if (posOpen !== -1) {
    return value.substring(0, posOpen);
  }
  return value;
}