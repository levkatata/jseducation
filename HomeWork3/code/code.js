let styles = ["Джаз", "Блюз"];
let list = document.getElementById("my-list");
console.log(`Array styles ${styles}`);
list.innerHTML = (`<li>${styles.join(', ')}</li>`);

styles.push("Рок-н-Ролл");
console.log(`Array styles ${styles}`);
list.innerHTML += (`<li>${styles.join(', ')}</li>`);

let middle = styles.length / 2;
if(Number.isInteger(middle) == false) {
    middle = Math.floor(middle);
}
styles[middle] = "Класика";
console.log(`Array styles ${styles}`);
list.innerHTML += (`<li>${styles.join(', ')}</li>`);

let first = styles.shift();
console.log(`The first element is ${first}`);
console.log(`Array styles ${styles}`);
list.innerHTML += (`<li>The first element is ${first}</li>`);
list.innerHTML += (`<li>${styles.join(', ')}</li>`);

styles.unshift("Реггі");
styles.unshift("Реп");
console.log(`Array styles ${styles}`);
list.innerHTML += (`<li>${styles.join(', ')}</li>`);
