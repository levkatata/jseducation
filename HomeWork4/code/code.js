// Задача 1
// Напиши функцию map(fn, array), которая принимает на вход функцию и массив, и 
// обрабатывает каждый элемент массива этой функцией, возвращая новый массив.

function map(fn, array) {
    const newAr = [];
    for(let el of array) {
        newAr.push(fn(el));
    }
    return newAr;
}

const testArray = [1, 3, 5, 7, 9, 11];
const incArray = (el) => {
    return ++el;
}
const powArray = (el) => {
    return (Math.pow(el, 2));
}


document.getElementById("origin-array").innerHTML = `Original array:<br/>${testArray.toString()}`;
document.getElementById("proccessed-array").innerHTML = `Increment every element:<br/>${map(incArray, testArray)}`;
document.getElementById("pow-array").innerHTML = `Pow every element:<br/>${map(powArray, testArray)}`;

// Задача 2
// Перепишите функцию, используя оператор '?' или '||'
// Следующая функция возвращает true, если параметр age больше 18. В ином случае она задаёт вопрос confirm и 
// возвращает его результат.
// 1 function checkAge(age) {
// 2 if (age > 18) {
// 3 return true;
// 4 } else {
// 5 return confirm('Родители разрешили?');
// 6 } }

function checkAge(age) {
    return age > 18 ? true : confirm("Родители разрешили?");
}

const userAge = Math.floor(Math.random() * 10 + 15);
console.log(userAge);
document.getElementById("age").innerHTML = `Вік користувача ${userAge}, checkAge(userAge) вертає ${checkAge(userAge)}`;