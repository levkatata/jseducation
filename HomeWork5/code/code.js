/*Напишіть функцію isEmpty(obj), яка повертає true, якщо об'єкт не має властивостей, інакше false.*/
function isEmpty(obj) {
    for (let p in obj) {
        return true;
    }
    return false;
}

let obj1 = {

};
let obj2 = {
    name: "Ivan",
    surname: "Ivanov",
    age : 18
};

let obj3  = {
    func : function() {
        console.log("Some function");
    }
}

console.log(`isEmpty(obj1) returns ${isEmpty(obj1)}`);
console.log(`isEmpty(obj2) returns ${isEmpty(obj2)}`);
console.log(`isEmpty(obj3) returns ${isEmpty(obj3)}`);

document.getElementById("obj1").innerHTML = `obj1 is [${Object.keys(obj1)}]<br/>isEmpty(obj1) returns ${isEmpty(obj1)}`;
document.getElementById("obj2").innerHTML = `obj2 is [${Object.keys(obj2)}]<br/>isEmpty(obj2) returns ${isEmpty(obj2)}`;
document.getElementById("obj3").innerHTML = `obj3 is [${Object.keys(obj3)}]<br/>isEmpty(obj3) returns ${isEmpty(obj3)}`;

/*Розробіть функцію-конструктор, яка буде створювати обєкт типу Human(людина). Створіть масив таких обєктів та реалізуйте 
функцію, яка буде сортувати елементи масива по значенню властивості age за більшенням або за зменшенням.
Додайте на свій вибір властивості та методи. Подумайте, які методи та властивості потрібно зробити на рівні екземпляру, а 
які на рівні функції-конструктора.
*/

function Human(name, surname, age, height) {
    this.name = name;
    this.surname = surname;
    this.age = age;
    this.height = height;

    if (Human.count === undefined) {
        Human.count = 0;
    }

    Human.newPerson = function () {
        Human.count++;
    }
    Human.getPersonCount = function () {
        return Human.count;
    }

    Human.newPerson();
}
Human.prototype.toString = function () {
    return `[name=${this.name}, surname=${this.surname}, age=${this.age}, height=${this.height}]`;
}
Human.prototype.getFullName = function() {
    return `${this.name} ${this.surname}`;
}

//Sort function, sort elements inside of original array
function sortHumans (arrHumans, byAsc = true) {
    for (let i = 0; i < arrHumans.length; i++) {
        for (let j = i + 1; j < arrHumans.length; j++) {
            if (byAsc) {
                if (arrHumans[i].age > arrHumans[j].age) {
                    let tmp = arrHumans[i];
                    arrHumans[i] = arrHumans[j];
                    arrHumans[j] = tmp;
                }
            } else {
                if (arrHumans[i].age < arrHumans[j].age) {
                    let tmp = arrHumans[i];
                    arrHumans[i] = arrHumans[j];
                    arrHumans[j] = tmp;
                }
            }
        }
    }
}

/*One more sort function, returns sorted array, do not affect original array*/
function returnSorted (arrHumans) {
    const res = arrHumans.map(x => x);
    
    for (let i = 0; i < arrHumans.length; i++) {
        let min = i;
        for(let j = i + 1; j < arrHumans.length; j++) {
            if (res[min].age > res[j].age) {
                min = j;
            }
        }
        if (min != i) {
            let a = res[i];
            res[i] = res[min];
            res[min] = a;
        }
    }
    return res;
}

const arrHumans = new Array(
    new Human("David", "Smith", 35, 175),
    new Human("Jack", "Baker", 50, 180),
    new Human("Rose", "Doson", 19, 170),
    new Human("Mark", "Nut", 5, 116),
    new Human("Monica", "Flower", 29, 163),
    new Human("Peter", "James", 11, 150));

console.log(`arrHumans before sorting: ${arrHumans}`);
document.getElementById("unsorted").innerHTML = `Unsorted: <br/> ${arrHumans}`;

sortHumans(arrHumans);

console.log(`arrHumans after sorting ascending: ${arrHumans}`);
document.getElementById("sorted-asc").innerHTML = `Sorting ascending: ${arrHumans}`;

sortHumans(arrHumans, false);

console.log(`arrHumans after sorting descending: ${arrHumans}`);
document.getElementById("sorted-desc").innerHTML = `Sorting descending: ${arrHumans}`;

console.log(`returnSorted ${returnSorted(arrHumans)}`);

console.log(Human.getPersonCount());

