/*Створити клас Car , Engine та Driver.
Клас Driver містить поля - ПІБ, стаж водіння.
Клас Engine містить поля – потужність, виробник.
Клас Car містить поля – марка автомобіля, клас автомобіля, вага, водій типу Driver, мотор типу Engine. 
Методи start(), stop(), turnRight(), turnLeft(), які виводять на друк: "Поїхали", "Зупиняємося", "Поворот праворуч" 
або "Поворот ліворуч". А також метод toString(), який виводить повну інформацію про автомобіль, її водія і двигуна.

Створити похідний від Car клас - Lorry (вантажівка), що характеризується також вантажопідйомністю кузова.
Створити похідний від Car клас - SportCar, який також характеризується граничною швидкістю. */

/*Інформація про автомобілі буде виводитися у консоль.*/
class Driver {
    firstName;
    lastName;
    yearsExp;

    constructor (firstName, lastName, yearsExp) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.yearsExp = yearsExp;
    }

    toString() {
        return `Driver: ${this.firstName} ${this.lastName}, ${this.yearsExp} years of experience`;
    }
}

class Engine {
    powerful; //poweful in a horse power
    production;

    constructor (powerful, production) {
        this.powerful = powerful;
        this.production = production;
    }

    toString() {
        return `Engine: powerful ${this.powerful} h.p., producer ${this.production}`;
    }
}

class Car {
    markAuto;
    classAuto;
    weigth;
    driver;
    engine;

    constructor (markAuto, classAuto, weigth, driver, engine) {
        this.markAuto = markAuto;
        this.classAuto = classAuto;
        this.weigth = weigth;
        this.driver = driver;
        this.engine = engine;
    }

    toString () {
        return `Car info: ${this.markAuto}, ${this.classAuto}, ${this.weigth} kg, Driver: ${this.driver.toString()}, Engine: ${this.engine.toString()}`;
    }

    start() {
        console.log("Поїхали!");
    } 
    
    stop() {
        console.log("Зупиняємося");
    } 
    
    turnRight(){
        console.log("Поворот праворуч");
    } 
    
    turnLeft() {
        console.log("Поворот ліворуч");
    }
}

class Lorry extends Car {
    carryCapacity;

    constructor (markAuto, classAuto, weigth, driver, engine, carryCapacity) {
        super(markAuto, classAuto, weigth, driver, engine);
        this.carryCapacity = carryCapacity;
    }

    toString () {
        return `Lorry ${super.toString()}, carry capacity ${this.carryCapacity} kg`;
    }
}

class SportCar extends Car {
    maxSpeed;

    constructor (markAuto, classAuto, weigth, driver, engine, maxSpeed) {
        super(markAuto, classAuto, weigth, driver, engine);
        this.maxSpeed = maxSpeed;
    }

    toString() {
        return `Sport ${super.toString()}, max speed ${this.maxSpeed} km/h`;
    }
}

const driver1 = new Driver("Ivan", "Smirnov", 10),
    driver2 = new Driver("Piter", "Pen", 25),
    driver3 = new Driver("David", "Baker", 10);

const engine1 = new Engine(178, "toyota"), 
    engine2 = new Engine(92, "pegout"),
    engine3 = new Engine(115, "renault"),
    engine4 = new Engine(85, "skoda"),
    engine5 = new Engine(200, "mazda"),
    engine6 = new Engine(250, "man");

const car1 = new Car("Toyota RAV4", "crossover", 1500, driver1, engine1),
    car2 = new Car("Pegout Parner", "sedan", 1130, driver2, engine2),
    car3 = new Car("Renault Traffic", "minibus", 1665, driver3, engine3);

const scar1 = new SportCar("Mazda MX-5", "sport car", 1000, driver3, engine5, 250);

const lcar1 = new Lorry("MAN lorry", "lorry van", 2000, driver1, engine6, 4000);

console.log(car1.toString());
car1.start();
car1.turnLeft();
car1.turnRight();
car1.stop();

console.log(car2.toString());
car2.start();
car2.turnLeft();
car2.turnRight();
car2.stop();

console.log(car3.toString());
car3.start();
car3.turnLeft();
car3.turnRight();
car3.stop();

console.log(scar1.toString());
scar1.start();
scar1.turnRight();
scar1.stop();

console.log(lcar1.toString());
lcar1.start();
lcar1.turnLeft();
lcar1.stop();
