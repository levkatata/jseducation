const btn = document.getElementById("btn");
const mainDiv = document.getElementById("main");
const panelDiv = document.getElementById("panel");

function getDiameter() {
    const textField = document.createElement("input");
    textField.type = "text";
    textField.placeholder = "Enter a diameter of circle (integer 5-100 px):";
    textField.style.width = "300px";
    textField.style.marginRight = "10px";
    panelDiv.prepend(textField);

    btn.innerText = "Start to draw";
    btn.onclick = () => {
        let diameter = parseFloat(textField.value);
        if(!isNaN(diameter) && !isNaN(diameter - 0) && diameter >= 5 && diameter <= 100) {
            console.log("Diameter is " + diameter);
            drawCircles(diameter);
        } else {
            alert("You entered a wrong number, must be an integer 5-100! Try again.");
            console.log("Error: " + diameter + ". Ask for a new number again.");
            textField.value = "";
        }
    }
}

function createRow(diameter) {
    const newRow = document.createElement("div");
    newRow.style.display = "flex";
    newRow.style.flexDirection = "row";
    newRow.style.justifyContent = "left";

    for(let i = 0; i < 10; i++) {
        const newCircle = document.createElement("div");
        newCircle.setAttribute("class", "div-circle");
        let color = Math.floor(Math.random()*360);
        console.log(color);
        newCircle.style.backgroundColor = `hsl(${color}, 100%, 50%)`;
        newCircle.style.width = `${diameter}px`;
        newCircle.style.height = `${diameter}px`;
        newCircle.onclick = () => {
            newCircle.remove();
        }
        newRow.append(newCircle);
    }
    return newRow;
}

function drawCircles(diameter) {
    if(mainDiv.childElementCount !== 0) {
        clearAll();
    }

    for (let i = 0; i < 10; i++) {
        mainDiv.append(createRow(diameter));
    }
}

function clearAll() {
    const [...allElements] = mainDiv.children;
    for(let e of allElements) {
        mainDiv.removeChild(e);
    }
}

btn.onclick = () => {
    getDiameter();
}

