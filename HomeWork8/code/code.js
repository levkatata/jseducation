function showPassword(e) {
    const eyeIcon = e.srcElement;
    const inputPass = eyeIcon.previousElementSibling;
    const isPassVisible = eyeIcon.isPassVisible;

    if (isPassVisible === undefined || isPassVisible === false) {
        eyeIcon.isPassVisible = true;
        inputPass.setAttribute("type", "text");
        eyeIcon.style.backgroundPositionY = "-27px";
    } else {
        eyeIcon.isPassVisible = false;
        inputPass.setAttribute("type", "password");
        eyeIcon.style.backgroundPositionY = "0px";
    }
    console.log(e);
}

function checkPassword(e) {
    console.log(e);
    if(inputPass1.value === inputPass2.value) {
        if(inputPass1.value === "") {
            alert("Passwords can not be empty!");
            return;
        }
        const elementError = document.getElementById(idLabelError);
        if (elementError !== null) {
            elementError.remove();
        }
        inputPass1.value = "";
        inputPass2.value = "";

        setTimeout(() => {
            alert("Passwords are equal! You are welcome!");
        }, 0); 
    } else {
        let elementError = document.getElementById(idLabelError);
        if (elementError === null) {
            elementError = document.createElement("div");
            elementError.id = idLabelError;
            elementError.innerText = errorMsg;
            elementError.classList.add("error-msg");
            btnCheck.parentNode.insertBefore(elementError, btnCheck);
        }
    }
}

const eyeIcon1 = document.getElementById("div-img1");
const eyeIcon2 = document.getElementById("div-img2");
const inputPass1 = document.getElementById("input-pass1");
const inputPass2 = document.getElementById("input-pass2");
const btnCheck = document.getElementById("btn-check-pass");
const idLabelError = "id-error";
const errorMsg = "Passwords are not equals";

eyeIcon1.onclick = showPassword;
eyeIcon2.onclick = showPassword;
btnCheck.onclick = checkPassword;