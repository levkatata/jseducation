/*Реалізуйте програму перевірки телефону
* Використовуючи JS Створіть поле для введення телефону та кнопку збереження;
* Користувач повинен ввести номер телефону у форматі 000-000-00-00;
* Після того як користувач натискає кнопку зберегти перевірте правильність введення номера, якщо номер 
    правильний  зробіть зелене тло і використовуючи document.location переведіть користувача на сторінку 
    https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg. 
    Якщо буде помилка, відобразіть її в діві до input.*/

const main = document.querySelector("main");

const divLabel = document.createElement("div");
const inputPhone = document.createElement("input");
const btnConfirm = document.createElement("button");
const labelErrorPhone = document.createElement("div");

const regExpPhoneCheck = /\d{3}-\d{3}-\d{2}-\d{2}$/g;
const errorMsg = "Phone number is not in format 000-000-00-00!";

divLabel.innerText = "Enter a phone number:";
divLabel.classList.add("label");
inputPhone.type = "tel";
inputPhone.placeholder = "000-000-00-00";
btnConfirm.innerText = "Confirm";
labelErrorPhone.innerText = "";
labelErrorPhone.classList.add("error");

btnConfirm.onclick = (e) => {
    console.log(inputPhone.value);
    const res = inputPhone.value.match(regExpPhoneCheck);
    console.log(res);
    if (res != null) {
        btnConfirm.disabled = true;

        main.classList.remove("red");
        main.classList.add("green");
        setTimeout(() => {
            document.location = "https://blog.insycle.com/hubfs/Why%20You%20Should%20Care%20About%20Phone%20Number%20Formatting%20In%20Your%20CRM.png";
        }, 1000);

        labelErrorPhone.innerText = "";
    } else {
        main.classList.remove("green");
        main.classList.add("red");
        
        labelErrorPhone.innerText = errorMsg;
    }
};

main.append(divLabel);
main.append(inputPhone);
main.append(labelErrorPhone);
main.append(btnConfirm);

