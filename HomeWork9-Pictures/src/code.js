/*Створіть слайдер кожні 3 сек змінюватиме зображення.*/

class Pictures {
    #urls = [
        "https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg",
        "https://naukatv.ru/upload/files/shutterstock_418733752.jpg",
        "https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg",
        "https://new-science.ru/wp-content/uploads/2020/03/4848-4.jpg",
        "https://nnst1.gismeteo.ru/images/2020/07/shutterstock_1450308851-640x360.jpg"
    ];
    #currentIndex = 0;
    constructor() {

    }
    getNextPicture() {
        let url = this.#urls[this.#currentIndex];
        if(++this.#currentIndex == this.#urls.length) {
            this.#currentIndex = 0;
        }
        console.log(url);
        console.log(this.#currentIndex);
        return `url(${url})`;
    }
}

const divPicture = document.querySelector("body > div");
const pictures = new Pictures();

setInterval(() => {
    divPicture.style.backgroundImage = pictures.getNextPicture();
    console.dir(divPicture.style.backgroundImage);
}, 3000);