/*Створіть на сторінці div і дайте йому зовнішній відступ 150 пікселів. Використовуючи JS виведіть у консоль відступ
Створіть програму секундомір.
* Секундомір матиме 3 кнопки "Старт, Стоп, Скидання"
* При натисканні на кнопку стоп фон секундоміра має бути червоним, старт - зелений, скидання - сірий *
 Виведення лічильників у форматі ЧЧ:ММ:СС
* Реалізуйте Завдання використовуючи синтаксис ES6 та стрілочні функції
*/

/*const div = document.createElement("div");
document.body.prepend(div); 
console.log(getComputedStyle(div).margin);*/
const btnStart = document.getElementById("start"), 
    btnStop = document.getElementById("stop"), 
    spanSec = document.getElementById("sec"),
    spanMin = document.getElementById("min"),
    spanHours = document.getElementById("hours"),
    btnReset = document.getElementById("reset");

const divStopwatch = document.querySelector(".container-stopwatch");

let interval = null;
let second = 0, hours = 0, minutes = 0;

btnStop.onclick = () => {
    if (interval != null) {
        clearClass();
        divStopwatch.classList.add("red");
        clearInterval(interval);
        interval = null;
    }
}

btnStart.onclick = () => {
    if (interval == null) {
        clearClass();
        divStopwatch.classList.add("green"); 

        /*second = 0;
        hours = 0;
        minutes = 0;*/
        interval = setInterval( ()=>{
            incrementSecond();
            showInterval();

        }, 1000);
    }
}

btnReset.onclick = () => {
    if (interval != null) {
        clearClass();
        divStopwatch.classList.add("silver");

        clearInterval(interval);
        interval = null;
    }
    second = 0;
    minutes = 0;
    hours  = 0;
    // set all spans to 00
    showInterval();
}

function clearClass() {
    divStopwatch.classList.remove("red", "black", "green", "silver");
}

function incrementSecond() {
    if (second == 59) {
        if (minutes == 59) {
            if (hours == 999) {
                // if time is 999:59:59 stop the timer, but leave values as they are.
                clearInterval(interval);
                interval = null;
                return;
            } else {
                hours++;
            }
            minutes = 0;
        } else {
            minutes++;
        }
        second = 0;
    } else {
        second++;
    }
}

function showInterval() {
    spanSec.innerText = proccessNumber(second);  
    spanMin.innerText = proccessNumber(minutes);
    spanHours.innerText = proccessNumber(hours);  
}

function proccessNumber(num) {
    return num < 10 ? `0${num}` : `${num}`;
}
