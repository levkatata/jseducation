import React from 'react';
import logo from './logo.svg';
import img1 from './logo192.png';
import './App.css';

const MyComponent = props => {
  return <div>My new component with {props.custom} argument!</div>;
};

class MyClassComponent extends React.Component {
  render() {
    return <h1>{this.props.text || 'default value text!!!'}</h1>;
  }
}

const classes = {
  prop1: 'property',
  prop2: 'one more property'
};
const element = <p className={classes.prop1} tabIndex='2'>Element JSX</p>;
const imgElement = <img src={img1} alg="image"/>


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          I have edited the App.js.
        </p>
        <MyComponent custom="1"/>
        <MyComponent custom="2"/>
        <MyClassComponent text="Text as arg"/>
        <MyClassComponent />
        {element}
        {imgElement}
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
