import './App.css';
import React, { Component } from 'react';
import Counters from './component/counters';
import NavBar from './component/navbar';

class App extends Component {
  state = {
    counters: [
        {id: 0, value: 4},
        {id: 1, value: 0},
        {id: 2, value: 3},
        {id: 3, value: 0},
        {id: 4, value: 5},
        {id: 5, value: 6}
    ]
  };

  handleDelete = (counterId) => {
    const newState = this.state.counters.filter((c) => {return c.id !== counterId});
    this.setState({ counters: newState });
  };

  handleIncrement = (counterId) => {
    const newState = this.state.counters.map((c) => {
        if (c.id === counterId) {
            c.value++;
        }
        return c;
    });
    this.setState({counters: newState});
  };

  handleReset = () => {
    const counters = this.state.counters.map(
        (c) => {
            c.value = 0;
            return c;
        });
    this.setState({counters});
};

constructor(props) {
  super(props);
}

render () {
    return (<React.Fragment>
      <NavBar totalNumbers={this.state.counters.filter(c => c.value !== 0).length}/>
      <main className='container'>
        <Counters 
          onDelete={this.handleDelete} 
          onIncrement={this.handleIncrement} 
          onReset={this.handleReset}
          counters={this.state.counters}/>
      </main>
    </React.Fragment>);
  }
}

export default App;
