import React, { Component } from 'react';

class Counter extends Component {
    // state = {
    //     counter : this.props.value,
    //     // tags : ["tag1", "tag2", "tag3"]
    // }

    // styleCus = {
    //     fontWeight: 900,
    //     fontSize: 20,
    //     color: 'red',
    //     margin: 30
    // }
    // <span style={{fontSize: 30, color: 'midnightblue'}} className={this.getBadgeStyle()}>{this.formatCounter()}</span>

    // incrementHandler = () => {
    //     //console.log("Increment", this.state.counter);
    //     this.setState({counter: this.state.counter + 1});
    // }

    render() { 
        return (<div>
            <span className={this.getBadgeStyle()}>{this.formatCounter()}</span>
            <button 
                className='btn btn-secondary btn-sm' 
                onClick={() => this.props.onIncrement(this.props.counter.id)}>
                    Increment
            </button>
            <button 
                className='badge badge-danger m-2' 
                onClick={() => this.props.onDelete(this.props.counter.id)}>
                    Delete
            </button>
        </div>);
    }

    formatCounter() {
        return this.props.counter.value === 0 ? 'Zero' : this.props.counter.value;
    }

    getBadgeStyle() {
        let styleName = 'badge m-2 badge-';
        styleName += this.props.counter.value === 0 ? 'warning' : 'primary';
        return styleName;
    }
}
 
export default Counter;