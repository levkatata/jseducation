import React, { Component } from 'react';

import Counter from './counter';

class Counters extends Component {
    constructor(props) {
        super(props);
    }
    render() { 
        return ( <div>
            <button className="btn btn-primary m-2 btn-sm"
                onClick={this.props.onReset}>Reset</button>
            {this.props.counters.map(c => 
                <Counter 
                    key={c.id} 
                    counter={c} 
                    onDelete={this.props.onDelete}
                    onIncrement={this.props.onIncrement}/>)}
        </div> );
    }
}
 
export default Counters;