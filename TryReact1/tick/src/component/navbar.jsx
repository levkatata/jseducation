import React, { Component } from 'react';

class NavBar extends Component {
    constructor(props) {
        super(props);
    }
    render() { 
        return (
            <nav className="navbar navbar-light bg-light">
                <span className="navbar-brand mb-0 h1">Navbar
                    <span className="badge badge-pill badge-secondary m-4">{this.props.totalNumbers}</span>
                </span>
            </nav>
        );
    }
}
 
export default NavBar;