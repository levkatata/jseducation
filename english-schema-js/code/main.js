const AIX_DID = "did", 
    AIX_WILL = "will", 
    AIX_DO = "do", 
    AIX_DOES ="does";

const TENSE_FUTURE = "id-tense-future",
    TENSE_PRESENT = "id-tense-present",
    TENSE_PAST = "id-tense-past";
const FORM_POS = "id-form-pos",
    FORM_NEG = "id-form-neg",
    FORM_QUES = "id-form-ques",
    FORM_W_QUEST = "id-form-w-quest";

class State {
    constructor(sentences) {
        this.tense = TENSE_PRESENT;
        this.form = FORM_POS;
        this.sentences = sentences;
        this.sentence = this.sentences[1];
    }

    setTense(tense) {
        this.tense = tense;
    }

    setForm(form) {
        this.form = form;
    }

    getSentence() {
        switch(this.tense) {
            case TENSE_FUTURE:
                if(this.form === FORM_POS) {
                    return this.sentence.getFutureSimpleForm();
                } else if (this.form === FORM_NEG) {
                    return this.sentence.getFutureSimpleFormNegative();
                } else if (this.form === FORM_QUES) {
                    return this.sentence.getFutureSimpleFormQuestion();
                } else if (this.form === FORM_W_QUEST) {
                    return this.sentence.getFutureSimpleFormSpQuestion();
                }
            break;
            case TENSE_PRESENT:
                if (this.form === FORM_POS) {
                    return this.sentence.getPresentSimpleForm();
                } else if (this.form === FORM_NEG) {
                    return this.sentence.getPresentSimpleFormNegative();
                } else if (this.form === FORM_QUES) {
                    return this.sentence.getPresentSimpleFormQuestion();
                } else if (this.form === FORM_W_QUEST) {
                    return this.sentence.getPresentSimpleFormSpQuestion();
                }
            break;
            case TENSE_PAST:
                if (this.form === FORM_POS) {
                    return this.sentence.getPastSimpleForm();
                } else if (this.form === FORM_NEG) {
                    return this.sentence.getPastSimpleFormNegative();
                } else if (this.form === FORM_QUES) {
                    return this.sentence.getPastSimpleFormQuestion();
                } else if (this.form === FORM_W_QUEST) {
                    return this.sentence.getPastSimpleFormSpQuestion();
                }
            break;
        }
    }
}

class Sentence {
    constructor(o) {
        this.subject = o.subject;
        this.pred_f1 = o.pred_f1;
        this.pred_f2 = o.pred_f2;
        this.compl = o.compl;
        this.pred_present = o.pred_present;
        this.q_word = o.q_word;

        this.divSentenceMain = document.createElement("div");
        this.divSubject = document.createElement("div");
        this.divAix = document.createElement("div");
        this.divVerb = document.createElement("div");
        this.divComplement = document.createElement("div");
        this.divQuestWord = document.createElement("div");

    }

    getPresentSimpleForm() {
        const sentenceCont = document.createElement("div"),
            divSubject = document.createElement("div"),
            // divAix = document.createElement("div"),
            divVerb = document.createElement("div"),
            divComplement = document.createElement("div");

        sentenceCont.classList.add("sentence");
        divSubject.classList.add("word", "subject");
        // divAix.classList.add("word", "auxiliary-word");
        divVerb.classList.add("word", "predicate");
        divComplement.classList.add("word", "complement");

        divSubject.innerText = this.toUpperFirstLetter(this.subject);
        divVerb.innerText = this.pred_present;
        divComplement.innerText = this.compl;

        sentenceCont.append(divSubject);
        sentenceCont.append(divVerb);
        sentenceCont.append(divComplement);

        return sentenceCont;
    }
    getPresentSimpleFormNegative() {
        return this.toUpperFirstLetter(this.subject) + " " + AIX_DO + " not " + this.pred_f1 + " " + this.compl + ".";
    }
    getPresentSimpleFormQuestion() {
        return this.toUpperFirstLetter(AIX_DO) + " " + this.subject + " " + this.pred_f1 + " " + this.compl + "?";
    }
    getPresentSimpleFormSpQuestion() {
        return this.q_word  + " " + AIX_DO + " " + this.subject + " " + this.pred_f1 + "?";
    }

    getPastSimpleForm() {
        return this.toUpperFirstLetter(this.subject) + " " + this.pred_f2 + " " + this.compl + ".";
    }
    getPastSimpleFormNegative() {
        return this.toUpperFirstLetter(this.subject) + " " + AIX_DID + " not " + this.pred_f1 + " " + this.compl + ".";
    }
    getPastSimpleFormQuestion() {
        return this.toUpperFirstLetter(AIX_DID) + " " + this.subject + " " + this.pred_f1 + " " + this.compl + "?";
    }
    getPastSimpleFormSpQuestion() {
        return this.q_word + " " + AIX_DID + " " + this.subject + " " + this.pred_f1 + "?";
    }

    getFutureSimpleForm() {
        return this.toUpperFirstLetter(this.subject) + " " + AIX_WILL + " " + this.pred_f1 + " " + this.compl + ".";
    }
    getFutureSimpleFormNegative() {
        return this.toUpperFirstLetter(this.subject) + " " + AIX_WILL + " not " + this.pred_f1 + " " + this.compl + ".";
    }
    getFutureSimpleFormQuestion() {
        return this.toUpperFirstLetter(AIX_WILL) + " " + this.subject + " " + this.pred_f1 + " " + this.compl + "?";
    }
    getFutureSimpleFormSpQuestion() {
        return this.q_word + " " + AIX_WILL + " " + this.subject + " " + this.pred_f1 + "?";
    }

    toUpperFirstLetter(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }
}

async function req(url) {
    const data = await fetch(url);
    return data.json();
}

let state;

document.addEventListener("DOMContentLoaded", () => {
    const divSentence = document.getElementById("sentence");
    req("./data/english-data.json").then(res => {
        // console.log(res);
        let sentences = res.sentences.map(o => {
            return new Sentence(o);
        });
        state = new State(sentences);

        // console.log(sentences);
        sentences.forEach(s => {
            console.log(
                s.getPresentSimpleForm(),
                s.getPresentSimpleFormNegative(),
                s.getPresentSimpleFormQuestion(),
                s.getPresentSimpleFormSpQuestion());

            console.log(s.getPastSimpleForm(),
                s.getPastSimpleFormNegative(),
                s.getPastSimpleFormQuestion(),
                s.getPastSimpleFormSpQuestion());

            console.log(s.getFutureSimpleForm(),
                s.getFutureSimpleFormNegative(),
                s.getFutureSimpleFormQuestion(),
                s.getFutureSimpleFormSpQuestion());
            });

            const [...radios] = document.querySelectorAll("input[type='radio']");
            radios.forEach(r => {
                const ch = r.checked;
                if (ch) {
                    changeState(r.id);
                    divSentence.innerHTML = state.getSentence();
                }
        
                r.addEventListener('change', (e) => {
                    changeState(e.target.id);
                    divSentence.innerHTML = state.getSentence();
                });
            });
        });

});

function changeState(id) {
    if (id.includes("form")) {
        state.setForm(id);
    } else if (id.includes("tense")) {
        state.setTense(id);
    }
}