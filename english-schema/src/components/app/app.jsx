import { createContext } from 'react';
import { useEffect, useState } from 'react';
import { FormSelect, options_form } from '../formselect/formselect';
import { Sentence } from '../sentense/sentence';
import { options_tense, TenseSelect } from '../tenseselect/tenseselect';
import styles from './app.module.css';
import { getCurrentSentence } from '../../helpers';

export const SentenceContext = createContext(null);

export function App() {
    const [ form, setForm ] = useState(options_form[0]);
    const [ tense, setTense ] = useState(options_tense[0]);
    const [ listSentences, setListSentences ] = useState([]);
    const [ curSentence, setCurSentence ] = useState(undefined);

    useEffect(() => {
        fetch('/sentences.json')
            .then(res => res.json())
            .then(res => {
                console.log(res);
                setListSentences(res.sentences);
                setCurSentence(getCurrentSentence(res.sentences));
            }, 
            (error) => {
                console.error(error);
            })
    }, []);

    const getSentence = () => {
        return listSentences[curSentence];
    }

    return (
        <SentenceContext.Provider value={{tense, form, curSentence, setTense, setForm}}>
            <main>
                {(listSentences.length !== 0) ?
                (<>
                    <div className={styles.divcontainer}>
                        <TenseSelect/>
                        {/* <div className={styles.divmock}/>   */}
                        <Sentence/>
                        <FormSelect/>
                    </div>
                </>)
                :
                (<><div>Loading...</div></>)
                }
            </main>
        </SentenceContext.Provider>
    );
}