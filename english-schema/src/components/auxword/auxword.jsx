import { Word } from '../word/word';
import styles from './auxword.module.css';

export function AuxiliaryWord({ value }) {
    return (<Word extraStyle={styles.auxword} value={value}/>);
}