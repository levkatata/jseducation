import React from "react";
import { Word } from "../word/word";
import styles from './complement.module.css';

export function Complement({ value }) {
    return (<Word 
        extraStyle={styles.complement}
        value={value}/>);
}