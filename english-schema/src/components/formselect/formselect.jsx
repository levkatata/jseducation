import React from "react";
import { useContext } from "react";
import { SentenceContext } from "../app/app";
import styles from './formselect.module.css';

export const options_form = ["+", "-", "?", "W?"];

export function FormSelect() {
   const {form, setForm} = useContext(SentenceContext);

   return <div className={styles.divmain}>
      <div className={styles.form_radio}>
         <label className={styles.label_container}>
            <input type="radio" name="form"
               checked={form === options_form[0]}
               onChange={(e) => {setForm(options_form[0])} }/>
            <span className={styles.toggle_radio}>Positive</span>
         </label> 
         <label className={styles.label_container}>
            <input type="radio" name="form"
               checked={form === options_form[1]}
               onChange={(e) => {setForm(options_form[1])} }/>
            <span className={styles.toggle_radio}>Negative</span>
         </label> 
         <label className={styles.label_container}>
            <input type="radio" name="form"
               checked={form === options_form[2]}
               onChange={(e) => {setForm(options_form[2])} }/>
            <span className={styles.toggle_radio}>Question</span>
         </label> 
         <label className={styles.label_container}>
            <input type="radio" name="form"
               checked={form === options_form[3]}
               onChange={(e) => {setForm(options_form[3])} }/>
            <span className={styles.toggle_radio}>Special question</span>
         </label> 
      </div>
   </div>;
}