import { Word } from "../word/word";
import styles from './marker.module.css';

export function Marker({value}) {
    return <Word extraStyle={styles.marker} value={value}/>;
}