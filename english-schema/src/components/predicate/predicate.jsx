import { Word } from "../word/word";
import styles from './predicate.module.css';

export function Predicate({ value }) {
    return <Word extraStyle={styles.predicate} 
        value={value}/>;
}