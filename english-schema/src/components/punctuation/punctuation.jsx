import { Word } from "../word/word";
import styles from './punctuation.module.css';

export function Punctuation ({value}) {
    return <Word extraStyle={styles.punctuation} 
        value={value} />;
}