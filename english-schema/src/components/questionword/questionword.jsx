import { Word } from "../word/word";
import styles from './questionword.module.css';

export function QuestionWord({ value }) {
    return (
        <Word extraStyle={styles.questionword}
            value={value}/>
    );
}