import React from 'react';
import { Subject } from '../subject/subject';
import { AuxiliaryWord } from '../auxword/auxword';
import { Predicate } from '../predicate/predicate';
import { Complement } from '../complement/complement';
import styles from './sentence.module.css';
import { options_tense } from '../tenseselect/tenseselect';
import { options_form } from '../formselect/formselect';
import { Punctuation } from '../punctuation/punctuation';
import { makeUpperFirstLetter, getPredicate, getAixWord, getSentenceMarker } from '../../helpers';
import { Marker } from '../marker/marker';
import { QuestionWord } from '../questionword/questionword';
import { useState } from 'react';
import { useContext } from 'react';
import { SentenceContext } from '../app/app';
import { useEffect } from 'react';

export function Sentence () {
    const { tense, form, curSentence } = useContext(SentenceContext);
    const [ state, setState ] = useState({
        q_word: "",
        aix_before: "",
        subject: "",
        aix_after: "",
        verb: "",
        complement: "",
        punctuation: ""
    });
    const [ marker, setMarker ] = useState("");

    useEffect(() => {
        setMarker(curSentence !== undefined ? getSentenceMarker(tense, curSentence) : "");
    }, [tense, curSentence]);

    const buildSentence = () => {
        //console.log(curSentence, tense);
        if (form === options_form[0]) {
            buildPositiveSentence();
        } else if (form === options_form[1]) {
            buildNegativeSentence();
        } else if (form === options_form[2]) {
            buildQuestion();
        } else if (form === options_form[3]) {
            buildWQuestion();
        }
    }

    const buildPositiveSentence = () => { 
        const { subject, complement, pred_f1 } = curSentence;

        setState({
            ...state,
            q_word: "",
            aix_before: "",
            subject: makeUpperFirstLetter(subject),
            aix_after: (pred_f1 === "be" ? getAixWord(tense, curSentence) : (tense === options_tense[0]) ? "will" : ""),
            verb: getPredicate(tense, form, curSentence),
            complement: complement,
            punctuation: "."
        });
    }

    const buildNegativeSentence = () => {
        const {subject, complement } = curSentence;

        setState({
            ...state,
            q_word: "",
            aix_before: "",
            subject: makeUpperFirstLetter(subject),
            aix_after: tense === options_tense[0] ? "won't" : getAixWord(tense, curSentence) + "n't",
            verb: getPredicate(tense, form, curSentence),
            complement: complement,
            punctuation: "."
        });
    }

    const buildQuestion = () => {
        const {subject, complement} = curSentence;

        setState({
            ...state,
            q_word: "",
            aix_before: makeUpperFirstLetter(getAixWord(tense, curSentence)),
            subject: subject !== "I" ? subject.toLowerCase() : subject,
            aix_after: "",
            verb: getPredicate(tense, form, curSentence), 
            complement: complement,
            punctuation: "?"
        });
    }

    const buildWQuestion = () => {
        const { q_word, subject } = curSentence;

        setState({
            ...state,
            q_word: q_word,
            aix_before: getAixWord(tense, curSentence),
            subject: subject !== "I" ? subject.toLowerCase() : subject,
            aix_after: "",
            verb: getPredicate(tense, form, curSentence),
            complement: "",
            punctuation: "?"
        });
    }

    useEffect(() => {
       buildSentence();
    }, [tense, form, curSentence])

    return (
        <div className={styles.sentence_container}>
            <div className={styles.sentence}>
                <QuestionWord value={state.q_word}/>
                <AuxiliaryWord value={state.aix_before}/>
                <Subject value={state.subject}/>
                <AuxiliaryWord value={state.aix_after}/>
                <Predicate value={state.verb}/>
                <Complement value={state.complement}/>
                <Marker value={marker}/>
                <Punctuation value={state.punctuation}/>
            </div>
        </div>);
}