import { Word } from '../word/word';
import styles from './subject.module.css';

export function Subject ({ value }) {
    return <Word extraStyle={styles.subject} 
        value={value}/>;
}
