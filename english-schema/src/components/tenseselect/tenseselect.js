import React, { useContext } from "react";
import { SentenceContext } from "../app/app";
import styles from './tenseselect.module.css'

export const options_tense = ["FS", "PrS", "PaS"];

export function TenseSelect() {
    const { tense, setTense } = useContext(SentenceContext);
 
    return <div className={styles.divmain}>
        <div className={styles.tense_radio}>
                <label className={styles.label_container}>
                    <input type="radio" 
                        name="tense"
                        checked={tense === options_tense[0]}
                        onChange={(e) => {setTense(options_tense[0])}}/>
                    <span className={styles.toggle_radio}>Future Simple</span>
                </label>
                <label className={styles.label_container}>
                    <input type="radio" 
                        name="tense"
                        checked={tense === options_tense[1]}
                        onChange={(e) => {setTense(options_tense[1])}}/>
                    <span className={styles.toggle_radio}>Present Simple</span>
                </label>
                <label className={styles.label_container}>
                    <input type="radio" 
                        name="tense"
                        checked={tense === options_tense[2]}
                        onChange={(e) => {setTense(options_tense[2])}}/>
                    <span className={styles.toggle_radio}>Past Simple</span>
                </label>
        </div>
    </div>;
}