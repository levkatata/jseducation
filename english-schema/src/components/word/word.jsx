import React, { useEffect, useRef, useState } from "react";
import {v4 as uuidv4} from 'uuid';
import styles from './word.module.css';

export function Word(props) {
    const prevValue = useRef(props.value);
    const [trueValue, setTrueValue] = useState(props.value);
    const [animation, setAnimation] = useState(""),
        [key, setKey] = useState("");
    const [curWidth, setCurWidth] = useState();

    const canvasRef = useRef();

    // useEffect(() => {
    //     console.log("useEffect with []");
    //     console.log(prevValue.current);
    //     console.log(trueValue);

    //     setCurWidth(calcCurWidth(canvasRef, props.value));
    //     // setTrueValue(props.value);    
    //     // prevValue.current = props.value;
    //     // setKey(uuidv4());
    // }, []);

    useEffect(() => {
        if (props.value !== "" && prevValue.current === "") {
            setAnimation(`${styles.animAppear}`);
        } else if (props.value === "" && prevValue.current !== "") {
            setAnimation(`${styles.animHide}`);
        } else if (props.value !== prevValue.current) {
            setAnimation(`${styles.animShake}`);
            setKey(uuidv4());
        } else {
            setAnimation('');
            setKey('');
        }

        setTimeout(() => {
            setTrueValue(props.value);    
            setCurWidth(calcCurWidth(canvasRef, props.value));
            prevValue.current = props.value;
        }, 1500);

    }, [props.value]);

    return (
        <>
            {trueValue !== "" 
            ? 
            (<div className={`${styles.word} ${animation} ${props.extraStyle}`}
                key={key}
                style={{width: `${Math.floor(curWidth)}px`}}>
                {trueValue}
            </div>) 
            : 
            (<div className={`${styles.noword}`}/>)}

            <canvas ref={canvasRef} style={{width: '0px', height: '0px'}}></canvas>
            
        </>
    );
}

function calcCurWidth(canvasRef, text) {
    const context = canvasRef.current.getContext('2d');
    context.font = "22px Arial";
    context.fontWeigth = "500";
    const curWidth = context.measureText(text).width;
    console.log("Measure text " + curWidth);
    return curWidth;
}