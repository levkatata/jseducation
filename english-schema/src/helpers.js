import { options_form } from "./components/formselect/formselect";
import { options_tense } from "./components/tenseselect/tenseselect";

export function getPresentBe(person, plural) {
    return (plural === "false" ? 
        (person === 1 ? "am" : (person === 2 ? "are" : "is")) 
        : 
        ("are") );
}
export function getPastBe(plural) {
    let res = (plural === "false" ? "was" : "were");
    console.log("getPastBe " + res);
    return res;
}
export function makeUpperFirstLetter(word) {
    return word[0].toUpperCase() + word.substr(1);
}
const prMarkers = ["sometimes", "always", "often", "usually", "every week", "every year", "every month"],
    pastMarkers = ["yesterday", "last year", "last evening", "a week ago", "a few monthes ago"],
    futureMarkers = ["tomorrow", "next year", "next week", "next evening", "following year"];

export function getPresentMarker() {
    return prMarkers[Math.floor(Math.random() * prMarkers.length)];
}
export function getPastMarker() {
    return pastMarkers[Math.floor(Math.random() * pastMarkers.length)];
}
export function getFutureMarker() {
    return futureMarkers[Math.floor(Math.random() * futureMarkers.length)];
}

export function getMarker(tense) {
    // console.log(tense);
    if (tense === options_tense[0]) {
        return getFutureMarker();
    } else if (tense === options_tense[1]) {
        return getPresentMarker();
    } else if (tense === options_tense[2]) {
        return getPastMarker();
    }
}
export function getSentenceMarker(tense, { marker_will, marker_was, marker_now }) {
    if (tense === options_tense[0]) {
        return marker_will;
    } else if (tense === options_tense[1]) {
        return marker_now;
    } else {
        return marker_was;
    }
}
export function getAixWord(tense, { person, plural, pred_f1 } ) {
    if (tense === options_tense[0]) {
        return "will";
    } else if (tense === options_tense[1]) {
        if (pred_f1 !== "be") {
            return person === 3 ? "does" : "do";
        } else {
            return person === 1 ? "am" : ((person === 3 && !plural) ? "is" : "are");
        }
    } else if (tense === options_tense[2]) {
        return pred_f1 !== "be" ? "did" : (plural ? "were" : "was");
    }
}
export function getPredicate(tense, form, { pred_f1, pred_f2, pred_fs, person, plural} ) {
 if (tense === options_tense[0]) {
        return pred_f1;
    } else if (tense === options_tense[1]) {
        if (pred_f1 === 'be') {
            // if (form === options_form[0]) {
            //     return getAixWord(tense, {person: person, plural: plural, pred_f1: pred_f1});
            // } else {
            //     return "";
            // }
            return "";
        } 

        if (person === 3 && form === options_form[0]) {
            return pred_fs;
        } else {
            return pred_f1;
        }
    } else if (tense === options_tense[2]) {
        if (pred_f1 === "be") {
            // if (form === options_form[0]) {
            //     return getAixWord(tense, {person: person, plural: plural, pred_f1: pred_f1});
            // } else {
            //     return "";
            // }
            return "";
        } else {
            return form === options_form[0] ? pred_f2 : pred_f1;
        }
    }
}

export function getCurrentSentence(jsonSentences) {
    const randomSentence = jsonSentences[Math.floor((jsonSentences.length - 1) * Math.random())];
    const trueSentence = {
        ...randomSentence,
        plural: randomSentence.plural === "true",
        person: parseInt(randomSentence.person)
    }
    console.log(trueSentence);
    return trueSentence;
}
